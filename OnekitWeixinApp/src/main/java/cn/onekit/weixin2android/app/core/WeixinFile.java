package cn.onekit.weixin2android.app.core;

import cn.onekit.weixin2android.Console;
import cn.onekit.weixin2android.WX;

public interface WeixinFile {
       WX wx = new WX();
       Console console = new Console();

       default Object onekit_or(Object condition, Object defaultValue) {
              if (condition != null) {
                     return condition;
              } else {
                     return defaultValue;
              }
       }

       default boolean onekit_logic(Object condition) {
              if (condition == null) {
                     return false;
              }
              if (condition instanceof Boolean) {
                     return (boolean) condition;
              }
              if (condition instanceof String) {
                     return !((String) condition).isEmpty();
              }
              return true;
       }
}

package demo.app

import cn.onekit.*
import cn.onekit.js.*
import cn.onekit.weixin2android.*;
import cn.onekit.weixin2android.app.*;

class app : WeixinApplication() {
    override fun onekit_js() {
        App(hashMapOf<String,Any?>(
            "onLaunch" to (object: function() { override fun invoke(arguments:Array<out Any?>):Any?{
                val logs = onekit_or(wx.getStorageSync("logs"),Array());
                (logs!!["unshift"] as function).invoke(Date.now());
                wx.setStorageSync("logs",logs);
                wx.login(hashMapOf<String,Any?>(
                    "success" to (object: function() {override fun invoke(arguments: Array<out Any?>): Any? {
                        return null
                    }})));
                return null
            }}),"globalData" to hashMapOf<String,Any?>(
                "userInfo" to null)));


    }
}
package cn.onekit.weixin2android.core;

import android.graphics.Canvas;

import java.util.Map;
import java.util.function.function;

import cn.onekit.ACTION1;
import cn.onekit.FUNC2;

public abstract class WxMedia extends WxCanvas {
    public Object createMapContext(String mapId, Object This) {
           return new Object();
    }
    public Object saveImageToPhotosAlbum(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object previewMedia(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object previewImage(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object getImageInfo(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object editImage(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object compressImage(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object chooseMessageFile(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object chooseImage(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object saveVideoToPhotosAlbum(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object openVideoEditor(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object getVideoInfo(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object createVideoContext(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object compressVideo(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object chooseVideo(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object chooseMedia(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object stopVoice(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object setInnerAudioOption(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object playVoice(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object pauseVoice(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object getAvailableAudioSources(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object createWebAudioContext(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object createMediaAudioPlayer(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object createInnerAudioContext(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object createAudioContext(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object stopBackgroundAudio(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object seekBackgroundAudio(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object playBackgroundAudio(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object pauseBackgroundAudio(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object onBackgroundAudioStop(function callback) {  return new Object();}
    public Object onBackgroundAudioPlay(function callback) {  return new Object();}
    public Object onBackgroundAudioPause(function callback) {  return new Object();}
    public Object getBackgroundAudioPlayerState(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object getBackgroundAudioManager() {   return new Object();}
    public Object createLivePusherContext(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object createLivePlayerContext(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object stopRecord(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object startRecord(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object getRecorderManager(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object createCameraContext() {  return new Object();}
    public Object createMediaContainer(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object updateVoIPChatMuteConfig(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object subscribeVoIPVideoMembers(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object setEnable1v1Chat(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object onVoIPVideoMembersChanged(function callback) {  return new Object();}
    public Object onVoIPChatStateChanged(function callback) {  return new Object();}
    public Object onVoIPChatSpeakersChanged(function callback) {  return new Object();}
    public Object onVoIPChatMembersChanged(function callback) {  return new Object();}
    public Object onVoIPChatInterrupted(function callback) {  return new Object();}
    public Object offVoIPVideoMembersChanged(function callback) {  return new Object();}
    public Object offVoIPChatStateChanged(function callback) {  return new Object();}
    public Object offVoIPChatMembersChanged(function callback) {  return new Object();}
    public Object offVoIPChatInterrupted(function callback) {  return new Object();}

    public Object joinVoIPChat(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object join1v1Chat(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object exitVoIPChat(Map<String,Object> object) {     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
    public Object createMediaRecorder(Canvas canvas, Map<String,Object> options ) { return new Object();}
    public Object createVideoDecoder( ) {   return new Object();}
}
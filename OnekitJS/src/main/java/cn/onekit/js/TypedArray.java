package cn.onekit.js;

import java.util.Collections;
import java.util.Comparator;

import cn.onekit.function;
import cn.onekit.js.core.Onekit_JS;


public abstract   class TypedArray<T> implements Iterable {

    private int _byteLength;
    private int _byteOffset;
    public ArrayBuffer _buffer;

    protected static <TA extends TypedArray> int _BYTES_PER_ELEMENT(Class<TA> clazz) {
        try {
            return (int) clazz.getField("BYTES_PER_ELEMENT").get(null);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
    protected static <TA extends TypedArray> String _name(Class<TA> clazz) {
        try {
            return (String) clazz.getField("name").get(null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("[");
        for (int i = 0; i < getLength(); i++) {
            if (i > 0) {
                result.append(",");
            }
            Object v = get(i);
            result.append(v);
        }
        result.append("]");
        return result.toString();
    }
    @Override
    public java.util.Iterator iterator() {
        return new java.util.Iterator() {
            int index = 0;

            @Override
            public boolean hasNext() {
                return index < getLength();
            }

            @Override
            public Object next() {
                return get(index++);
            }
        };
    }
    ///////////////////////////////////////////////////

    public Object get(Integer index) {
        int size=_BYTES_PER_ELEMENT(getClass());
        return  Onekit_JS.bytes2number(_buffer._data,_name(getClass()).replace("Array",""), size,size*index);
    }

    public void set(Object index, Object value) {
        _set(Onekit_JS.number(index,0,0).intValue(),this,value);
    }

    public static int _index(TypedArray array, int index) {
        if (index >= 0) {
            return index;
        }
        return array.getLength() + index;
    }
    ////////////////////////////////////////////////////////


    public <TA extends TypedArray> TypedArray(Class<TA> clazz, Object arg) {
        /*if(arg instanceof NUMBER){
            int len=OnekitJS.number(arg,0,0).intValue()* _BYTES_PER_ELEMENT(clazz);
            this(clazz, new ArrayBuffer(new NUMBER(len)),new NUMBER(arg));
        }else if(arg instanceof ArrayBuffer){
            this(clazz, arg, new NUMBER(0));
        }*/
    }

    public <TA extends TypedArray>  TypedArray(TA typedArray) {
        this(typedArray.getClass(),new ArrayBuffer(typedArray.getBuffer()._data.clone()));
    }

    public <TA extends TypedArray> TypedArray(Class<TA> clazz, Object buffer, Object byteOffset, Object length) {
        this._byteOffset = Onekit_JS.number(byteOffset,0,0).intValue();
        this._buffer = (ArrayBuffer) buffer;
        this._byteLength = Onekit_JS.number(length,0,0).intValue();
    }
    public <TA extends TypedArray> TypedArray(Class<TA> clazz, Object buffer, Object byteOffset) {
        this(clazz, buffer, byteOffset, (((ArrayBuffer)buffer).getByteLength() - (int)byteOffset));
    }

    public <TA extends TypedArray> TypedArray(Class<TA> clazz, Array array) {
        this(clazz, _array2buffer(clazz,array));
    }
    public <TA extends TypedArray> TypedArray(Class<TA> clazz, ArrayBuffer buffer) {
        this(clazz, buffer,0);
    }
    private static < TA extends TypedArray> ArrayBuffer _array2buffer(Class<TA> clazz, Array array){
        ArrayBuffer result=new ArrayBuffer(array.size()*_BYTES_PER_ELEMENT(clazz));

        for(int i = 0; i< array.size(); i++){
            Onekit_JS.number2bytes(result._data,clazz.getSimpleName().replace("Array",""),_BYTES_PER_ELEMENT(clazz),_BYTES_PER_ELEMENT(clazz)*i, _fix(clazz,array.get(i)));

        }
        return result;
    }
    ////////////////////////////////////////////////////

    public ArrayBuffer getBuffer() {
        return _buffer;
    }

    public int getByteLength() {
        return _byteLength;
    }

    public int getByteOffset() {
        return _byteOffset;
    }

    public int getLength() {
        int size = _BYTES_PER_ELEMENT(this.getClass());
        return getByteLength() / size;
    }
    private static  Object _fix(Class clazz,Object flag){
        if(flag.equals(Double.NaN)) {
            return 0.0;
        }
        String str = flag.toString();
        String name = clazz.getSimpleName().replace("Array", "");
        double value;
        switch (name) {
            case "Int8":
            case "Uint8Clamped":
            case "Uint8":
            case "Int16":
            case "Uint16":
            case "Int32":
                value= Double.valueOf(str).intValue();break;
            case "Uint32":
                value= Long.valueOf(str);break;
            case "Float32":
                value= Float.valueOf(str);break;
            case "Float64":
                value= Double.valueOf(str);break;
            default:
                throw new Error(new String(name));
        }
        return value;
    }
    public static <TA extends TypedArray> void _set(int index, TA typedArray, Object flag) {
        Class clazz = typedArray.getClass();


        int size=_BYTES_PER_ELEMENT(clazz);
        Onekit_JS.number2bytes(typedArray._buffer._data,_name(clazz).replace("Array",""),size ,index*size, _fix(clazz,flag));
    }

    protected static <TA extends TypedArray>  TA _from(Class<TA> clazz, Object source, Object mapFn, Object thisArg) {
        if(source instanceof Set) {
            return _from(clazz,(Set)source, mapFn, thisArg);
        }else  if(source instanceof String) {
            return _from(clazz, (String)source, mapFn, thisArg);
        }else if(source instanceof Array) {
            return _from(clazz, (Array)source, mapFn, thisArg);
        }else{
            return null;
        }
    }
    private static <TA extends TypedArray> TA _from(Class<TA> clazz, Set source, function mapFn, Object thisArg) {
        try {

            ArrayBuffer buffer = new ArrayBuffer(source.getSize() * _BYTES_PER_ELEMENT(clazz));
            TA result = clazz.getConstructor(ArrayBuffer.class).newInstance(buffer);
            int i = 0;
            for (Object element : source) {
                Object flag;
                if (mapFn != null) {
                    flag = mapFn.invoke(element);
                } else  {
                    flag=element;
                }

               _set(i++,result,flag);
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static <TA extends TypedArray>  TA _from(Class<TA> clazz, Array source, function mapFn, Object thisArg) {
        return _from(clazz, new Set(source),mapFn,thisArg);
    }
    private static <TA extends TypedArray>  TA _from(Class<TA> clazz, String source, function mapFn, Object thisArg) {
  Array array = Onekit_JS.string2Array(source);
    return _from(clazz, new Set(array),mapFn,thisArg);
    }

    public static < TA extends TypedArray> TA _of(Class<TA> clazz, Object... elements) {
        try {

            ArrayBuffer buffer = new ArrayBuffer(elements==null?1:elements.length * _BYTES_PER_ELEMENT(clazz));
            TA result = clazz.getConstructor(ArrayBuffer.class).newInstance(buffer);
            if (elements == null) {
                result._buffer = new ArrayBuffer(1 * result._BYTES_PER_ELEMENT(clazz));
                result.set(0,0);
            } else {
                result._buffer = new ArrayBuffer(elements.length * result._BYTES_PER_ELEMENT(clazz));
                for (int i = 0; i < elements.length; i++) {
                    Object element = elements[i];
                    if(element==null){
                        element=0;
                    }
                    _set(i,result,element);
                }
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    ////////////////////////////////////////////////////
    public <TA extends TypedArray> TA copyWithin(Object target, Object start, Object end) {
        if(end==null){
            end =  this.getLength();
        }
        int t = Onekit_JS.number(target,0,0).intValue();
        int s = Onekit_JS.number(start,0,0).intValue();
        int e = Onekit_JS.number(end,0,0).intValue();
        for (int i = s, j = t; i < e && i<getLength() && j<getLength(); i++, j++) {
            Object element = get(i);
            set(j, element);
        }
        return (TA) this;
    }


        public Boolean every(function callback, Object thisArg)  {
            callback.thisArg = thisArg;
            for (int i = 0; i < getLength(); i++) {
                Object element = get(i);
                if (element == null) {
                    continue;
                }

                if (!Onekit_JS.is(callback.invoke(element, i, this))) {
                    return new Boolean(false);
                }
            }
            return new Boolean(true);
        }

        public Boolean every(function callback) {
            return every(callback, null);
        }

        //
        <TA extends TypedArray> TA _fill(Object value, int start, int end) {
            start = _index(this, start);
            end = _index(this, end);
            for (int i = start; i >= 0 && i < end && i < getLength(); i++) {
                set( i, value);
            }
            return (TA) this;
        }

        public <TA extends TypedArray> TA fill(Object value, Object start, Object end) {
            int start_ = Onekit_JS.number(start, 0, 0).intValue();
            int end_ = Onekit_JS.number(end, 0, 0).intValue();
            return _fill(value, start_, end_);
        }

        public <TA extends TypedArray> TA fill(Object value, Object start) {
            return fill(value, start, getLength());
        }

        public <TA extends TypedArray> TA fill(Object value) {
            return fill(value,  0);
        }

        public <TA extends TypedArray>  TA filter(function callback, Object thisArg) {

            callback.thisArg = thisArg;
            Array array = new Array();
            for (int i = 0; i < getLength(); i++) {
                Object element = this.get(i);
                if (element == null) {
                    continue;
                }
                if (Onekit_JS.is(callback.invoke(element, i, this))) {
                    array.add(element);
                }
            }
            Class<TA> clazz = (Class<TA>) getClass();
            return TA._of(clazz, array);
        }

        public <TA extends TypedArray> TA filter(function callback) {
            return filter(callback, null);
        }

        public  Object find(function callback, Object thisArg) {
            callback.thisArg = thisArg;
            for (int i = 0; i < getLength(); i++) {
                Object element = this.get(i);
                if (Onekit_JS.is(callback.invoke(element, i, this))) {
                    return element;
                }
            }
            return null;
        }

        public  Object find(function callback) {
            return find(callback, null);
        }

        public int findIndex(function callback, Object thisArg) {
            callback.thisArg = thisArg;
            for (int i = 0; i < getLength(); i++) {
                Object element = this.get(i);
                if (element == null) {
                    continue;
                }
                if (Onekit_JS.is( callback.invoke(element, i, this)) ){
                    return i;
                }
            }
            return  -1;
        }

        public int findIndex(function callback) {
            return findIndex(callback, null);
        }

        public  void forEach(function callback, Object thisArg) {
            callback.thisArg = thisArg;
            for (int i = 0; i < getLength(); i++) {
                Object element = this.get(i);
                if (element == null) {
                    continue;
                }
                callback.invoke(element, i, this);
            }

        }

        public void forEach(function callback) {
            forEach(callback, null);
        }

        //
        public Boolean includes(Object valueToFind, Object index) {
        int idx = Onekit_JS.number(index,0,0).intValue();
            double target;
            if (Onekit_JS.isNumber(valueToFind)) {
                target = Onekit_JS.number(valueToFind,0,0).doubleValue();
            } else {
                target = 0;
            }
            for (int i = idx; i < getLength(); i++) {
                double element =  Onekit_JS.number(this.get(i),0,0).doubleValue();
                if (target == element) {
                    return new Boolean( true);
                }
            }

            return  new Boolean( false);
        }

        public Boolean includes(Object valueToFind) {
            return includes(valueToFind, 0);
        }

        //
        public int indexOf(Object searchElement, int fromIndex) {
            double target;
            if(Onekit_JS.isNumber(searchElement)){
                target = Onekit_JS.number(searchElement,0,0).doubleValue();
            }else{
                target = 0;
            }
            fromIndex = _index(this, fromIndex);
            for (int i = fromIndex; i < getLength(); i++) {
                double element = Onekit_JS.number( get(i),0,0).doubleValue();
                if (target==element) {
                    return i;
                }
            }
            return   -1;
        }

        public int indexOf(Object searchElement) {
            return indexOf(searchElement, 0);
        }


        public String _join(String separator) {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < getLength(); i++) {
                if (i > 0) {
                    result.append(separator);
                }
                result.append(get(i));
            }
            return result.toString();
        }

        public String join(Object separator) {
            if (separator == null) {
                separator = new String(",");
            }
            return new String(_join(separator.toString()));
        }

        public int lastIndexOf(Object searchElement, Integer fromIndex) {
            if (fromIndex == null) {
                fromIndex = getLength() - 1;
            }
            double target;
            if (Onekit_JS.isNumber(searchElement)) {
                target = Onekit_JS.number(searchElement, 0, 0).doubleValue();
            } else {
                target = 0;
            }
            int index = _index(this, fromIndex);
            for (int i = index; i >= 0; i--) {
                double element = Onekit_JS.number(get(i), 0, 0).doubleValue();
                if (element == target) {
                    return i;
                }
            }
            return -1;
        }


        //
        public <TA extends TypedArray>  TA map(function callback, Object thisArg) {
            callback.thisArg = thisArg;
            Array array = new Array();
            for (int i = 0; i < getLength(); i++) {
                Object element = this.get(i);
                if (element == null) {
                    continue;
                }
                Class<TA> clazz = (Class<TA>)getClass();
                Object item = callback.invoke(element);
                array.add(_fix(clazz,item));
            }
            Class<TA> clazz = (Class<TA>) getClass();
            return TA._of(clazz, array.toArray(new Object[0]));
        }

        public <TA extends TypedArray> TA map(function callback) {
            return map(callback, null);
        }


        public  Object reduce(function callback, Object initialValue) {
            if (initialValue == null) {
                initialValue =  get(0);
            }
            for (int i = 0; i < getLength(); i++) {
                Object element = this.get(i);
                if (element == null) {
                    continue;
                }
                initialValue = callback.invoke(initialValue, element,i, this);
            }
            return initialValue;
        }

        public  Object reduce(function callback) {
            return reduce(callback, null);
        }

        public  Object reduceRight(function callback, Object initialValue) {
            if (initialValue == null) {
                initialValue =  get(0);
            }
            for (int i = getLength() - 1; i >= 0; i--) {
                Object element = this.get(i);
                if (element == null) {
                    continue;
                }
                initialValue = callback.invoke(initialValue, element, i, this);
            }
            return initialValue;
        }

        public  Object reduceRight(function callback) {
            return reduceRight(callback, null);
        }

        public <TA extends TypedArray> TA reverse() {

            Array array = new Array();
            for (int i = getLength() - 1; i >= 0; i--) {
                array.add(get(i));
            }
            for (int i = 0; i < array.size(); i++) {
                set(i, array.get(i));
            }
            return (TA) this;
        }

    public void set(Array array, Object offset) {
        int o = Onekit_JS.number(offset,0,0).intValue();
        for (int i = 0, j = o; i < array.size(); i++, j++) {
            set(j, array.get(i));
        }
    }
    public void set(TypedArray typedArray, Object offset) {
        int o = Onekit_JS.number(offset,0,0).intValue();
        for (int i = 0, j = o; i < typedArray.getLength(); i++, j++) {
            set(j, typedArray.get(i));
        }
    }

    public <TA extends TypedArray> TA _slice(int start, int end) {
        start = _index(this,start);
        end = _index(this,end);
        Array array = new Array();
        for (int i = start; i < end; i++) {
            array.add(get(i));
        }
        Class<TA> clazz = (Class<TA>) getClass();
        return TA._of(clazz, array);
    }

    public <TA extends TypedArray> TA slice(Object start, Object end) {
        int start_ = Onekit_JS.number(start, 0, 0).intValue();
        int end_ = Onekit_JS.number(end, 0, 0).intValue();
        return _slice(start_, end_);
    }

    public <TA extends TypedArray> TA slice(Object start) {
        return slice(start, getLength());
    }

    public <TA extends TypedArray> TA slice() {
        return slice(0);
    }

    public Boolean some(function callback, Object thisArg) {
        for (int i = 0; i < getLength(); i++) {
            Object element = this.get(i);
            if (element == null) {
                continue;
            }
            if (Onekit_JS.is(callback.invoke(element, i, this))) {
                return new Boolean( true);
            }
        }
        return new Boolean( false);
    }

    public <TA extends TypedArray> TA sort(function comparefunction) {
        Array array = new Array();
        for (int i = 0; i < getLength(); i++) {
            array.add(get(i));
        }
        Collections.sort(array, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return ((int)comparefunction.invoke((Object) o1, (Object) o2));
            }
        });
        for (int i = 0; i < array.size(); i++) {
            set(i,array.get(i));
        }
        return (TA) this;
    }

    public  <TA extends TypedArray> TA sort() {
        return sort(new function() {
            @Override
            public Object invoke(Object... arguments) {
                Double v1 = Onekit_JS.number(arguments[0],0,0).doubleValue();
                Double v2 =  Onekit_JS.number(arguments[1],0,0).doubleValue();
                return v1.compareTo(v2);
            }
        });
    }
    public  <TA extends TypedArray> TA subarray(int begin, int end){
        Array array = new Array();
        for (int i = begin; i < end; i++) {
            array.add(get(i));
        }
        Class<TA> clazz = (Class<TA>) getClass();
        return TA._of(clazz, array);
    }
    public  <TA extends TypedArray> TA subarray(int begin){
        return subarray(begin,getLength());
    }
    public  <TA extends TypedArray> TA subarray(){
        return subarray(0);
    }



}

package cn.onekit.weixin2android.core;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import cn.onekit.ACTION1;
import cn.onekit.FUNC2;

public abstract class Wx {
public Object get(String key){
    for(Method method : this.getClass().getMethods()){
        if(method.getName().equals(key)){
            return method;
        }
    }
    return null;
}

    protected Object PROMISE(FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object> body,
                          ACTION1<Map<String, Object>> success,
                          ACTION1<Map<String, Object>> fail,
                          ACTION1<Map<String, Object>> complete) {
        try {
            return body.invoke((res) -> {
                if (success != null) {
                    success.invoke(res);
                }
                if (complete != null) {
                    complete.invoke(res);
                }
            }, (res) -> {
                if (fail != null) {
                    fail.invoke(res);
                }
                if (complete != null) {
                    complete.invoke(res);
                }
            });
        } catch (Exception e) {
            Map<String, Object> error = new HashMap<String, Object>() {{
                put("errMsg", e.getMessage());
            }};
            if (fail != null) {
                fail.invoke(error);
            }
            if (complete != null) {
                complete.invoke(error);
            }
            return null;
        }
    }
}

package cn.onekit.weixin2android.app;

import android.content.Context;
import android.util.AttributeSet;

import java.util.HashMap;
import cn.onekit.w3c.Event;
import cn.onekit.w3c.core.FormItem_;
import cn.onekit.weixin2android.app.WeixinElement;
import cn.onekit.weixin2android.app.core.ui.CoreCheckBox;

public class CheckBox extends WeixinElement implements FormItem_<Boolean> {

    public CheckBox(Context context) {
        super(context);
    }
    public CheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private void _init() {
        inflate(getContext(), R.layout.onekit_switch, this);
        setColor(getColor());
        _checkbox()._checkbox().setOnCheckedChangeListener((sender, isChecked) -> {

            Event event = new Event("change",new HashMap<String,Object>() {{

                put("value",new Boolean( isChecked));

            }},this,this,0);
            dispatchEvent(event);
        });
//

    }
    private CoreCheckBox _checkbox() {
        return  this.findViewById(R.id.checkbox);
    }
private String _name;
    @Override
    public void setName(String name) {
_name=name;
    }

    @Override
    public String getName() {
        return _name;
    }

    @Override
    public void setValue(Boolean value) {
_value=value;
    }

    Boolean _value;
    @Override
    public Boolean getValue() {

        return _checkbox().getChecked()?_value:null;
    }

    @Override
    public void reset() {
        _checkbox().setChecked(false);
    }

    int _color;

    public void setColor(int color) {
        _color = color;

        _checkbox().setColor(_color);

    }

    public int getColor() {
        return _color;
    }
    private Boolean _checked;
    public void setChecked(Boolean checked){
        _checked=checked;
        _checkbox().setChecked(checked);
    }
    public Boolean getChecked(){
        return _checked;
    }



}

package cn.onekit.weixin2android.core.wx.basic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import cn.onekit.function;

import cn.onekit.js.ArrayBuffer;
import cn.onekit.weixin2android.RealtimeLogManager;
import cn.onekit.weixin2android.UserCryptoManager;
import cn.onekit.ACTION1;
import cn.onekit.FUNC2;
import cn.onekit.weixin2android.core.wx.Wx;

public abstract class WxBase extends Wx {
    public Map<String, String> getEnv() {
        return new HashMap<>();
    }

    public boolean getCanIUse(String schema) {
        return true;
    }

    public ArrayBuffer base64ToArrayBuffer(String base64) {
        return null;
    }

    public String arrayBufferToBase64(ArrayBuffer arrayBuffer) {
        return null;
    }


}

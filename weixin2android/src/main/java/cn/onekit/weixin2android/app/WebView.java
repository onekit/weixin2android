package cn.onekit.weixin2android.app;

import android.content.Context;
import android.util.AttributeSet;

public class WebView extends WeixinElement {
    public WebView(Context context) {
        super(context);
    }

    public WebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    protected void onLayout(boolean b, int i, int i1, int i2, int i3) {

    }
}

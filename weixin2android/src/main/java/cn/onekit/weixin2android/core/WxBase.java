package cn.onekit.weixin2android.core;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.function;

import cn.onekit.js.ArrayBuffer;
import cn.onekit.weixin2android.RealtimeLogManager;
import cn.onekit.weixin2android.UserCryptoManager;
import cn.onekit.ACTION1;
import cn.onekit.FUNC2;

public abstract class WxBase extends Wx {
    public Map<String, String> getEnv() {
        return new HashMap<>();
    }

    public boolean getCanIUse(String schema) {
        return true;
    }

    public ArrayBuffer base64ToArrayBuffer(String base64) {
        return null;
    }

    public String arrayBufferToBase64(ArrayBuffer arrayBuffer) {
        return null;
    }

    public Object openSystemBluetoothSetting(Map<String, Object> object) {
        ACTION1<Map<String, Object>> success = (object != null && object.containsKey("success")) ? (ACTION1<Map<String, Object>>) object.get("success") : null;
        ACTION1<Map<String, Object>> fail = (object != null && object.containsKey("fail")) ? (ACTION1<Map<String, Object>>) object.get("fail") : null;
        ACTION1<Map<String, Object>> complete = (object != null && object.containsKey("complete")) ? (ACTION1<Map<String, Object>>) object.get("complete") : null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        }, success, fail, complete);
        return new Object();
    }

    public Object openAppAuthorizeSetting(Map<String, Object> object) {
        ACTION1<Map<String, Object>> success = (object != null && object.containsKey("success")) ? (ACTION1<Map<String, Object>>) object.get("success") : null;
        ACTION1<Map<String, Object>> fail = (object != null && object.containsKey("fail")) ? (ACTION1<Map<String, Object>>) object.get("fail") : null;
        ACTION1<Map<String, Object>> complete = (object != null && object.containsKey("complete")) ? (ACTION1<Map<String, Object>>) object.get("complete") : null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        }, success, fail, complete);
        return new Object();
    }

    public Object getWindowInfo() {
        return new Object();
    }

    public Object getSystemSetting() {
        return new Object();
    }

    public Map<String,Object> getSystemInfoSync() {
        return new HashMap<>();
    }

    public Object getSystemInfoAsync(Map<String, Object> object) {
        ACTION1<Map<String, Object>> success = (object != null && object.containsKey("success")) ? (ACTION1<Map<String, Object>>) object.get("success") : null;
        ACTION1<Map<String, Object>> fail = (object != null && object.containsKey("fail")) ? (ACTION1<Map<String, Object>>) object.get("fail") : null;
        ACTION1<Map<String, Object>> complete = (object != null && object.containsKey("complete")) ? (ACTION1<Map<String, Object>>) object.get("complete") : null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        }, success, fail, complete);
        return new Object();
    }

    public Object getSystemInfo(Map<String, Object> object) {
        ACTION1<Map<String, Object>> success = (object != null && object.containsKey("success")) ? (ACTION1<Map<String, Object>>) object.get("success") : null;
        ACTION1<Map<String, Object>> fail = (object != null && object.containsKey("fail")) ? (ACTION1<Map<String, Object>>) object.get("fail") : null;
        ACTION1<Map<String, Object>> complete = (object != null && object.containsKey("complete")) ? (ACTION1<Map<String, Object>>) object.get("complete") : null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        }, success, fail, complete);
        return new Object();
    }

    public Object getDeviceInfo() {
        return new Object();
    }

    public Object getAppBaseInfo() {
        return new Object();
    }

    public Object getAppAuthorizeSetting() {
        return new Object();
    }

    public Object updateWeChatApp(Map<String, Object> object) {
        ACTION1<Map<String, Object>> success = (object != null && object.containsKey("success")) ? (ACTION1<Map<String, Object>>) object.get("success") : null;
        ACTION1<Map<String, Object>> fail = (object != null && object.containsKey("fail")) ? (ACTION1<Map<String, Object>>) object.get("fail") : null;
        ACTION1<Map<String, Object>> complete = (object != null && object.containsKey("complete")) ? (ACTION1<Map<String, Object>>) object.get("complete") : null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        }, success, fail, complete);
        return new Object();
    }

    public Object getUpdateManager() {
        return new Object();
    }

    public Object getLaunchOptionsSync() {
        return new Object();
    }

    public Object getEnterOptionsSync() {
        return new Object();
    }

    public Object onUnhandledRejection(function callback) {
        return new Object();
    }

    public Object onThemeChange(Object object) {
        return new Object();
    }

    public Object onPageNotFound(function callback) {
        return new Object();
    }

    public Object onLazyLoadError(function callback) {
        return new Object();
    }

    public Object onError(function callback) {
        return new Object();
    }

    public Object onAudioInterruptionEnd(function callback) {
        return new Object();
    }

    public Object onAudioInterruptionBegin(function callback) {
        return new Object();
    }

    public Object onAppShow(function callback) {
        return new Object();
    }

    public Object onAppHide(function callback) {
        return new Object();
    }

    public Object offUnhandledRejection(function callback) {
        return new Object();
    }

    public Object offThemeChange(function callback) {
        return new Object();
    }

    public Object offPageNotFound(function callback) {
        return new Object();
    }

    public Object offError(function callback) {
        return new Object();
    }

    public Object offAudioInterruptionEnd(function callback) {
        return new Object();
    }

    public Object offAudioInterruptionBegin(function callback) {
        return new Object();
    }

    public Object offAppShow(function callback) {
        return new Object();
    }

    public Object offAppHide(function callback) {
        return new Object();
    }

    public Object setEnableDebug(Map<String, Object> object) {
        ACTION1<Map<String, Object>> success = (object != null && object.containsKey("success")) ? (ACTION1<Map<String, Object>>) object.get("success") : null;
        ACTION1<Map<String, Object>> fail = (object != null && object.containsKey("fail")) ? (ACTION1<Map<String, Object>>) object.get("fail") : null;
        ACTION1<Map<String, Object>> complete = (object != null && object.containsKey("complete")) ? (ACTION1<Map<String, Object>>) object.get("complete") : null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        }, success, fail, complete);
        return new Object();
    }

    public RealtimeLogManager getRealtimeLogManager() {
        return new RealtimeLogManager();
    }

    public Object getLogManager(Map<String, Object> object) {
        ACTION1<Map<String, Object>> success = (object != null && object.containsKey("success")) ? (ACTION1<Map<String, Object>>) object.get("success") : null;
        ACTION1<Map<String, Object>> fail = (object != null && object.containsKey("fail")) ? (ACTION1<Map<String, Object>>) object.get("fail") : null;
        ACTION1<Map<String, Object>> complete = (object != null && object.containsKey("complete")) ? (ACTION1<Map<String, Object>>) object.get("complete") : null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        }, success, fail, complete);
        return new Object();
    }

    public Object reportPerformance(Number id, Number value, String dimensions) {
        return new Object();
    }

    public Object reportPerformance(Number id, Number value, List<Object> dimensions) {
        return new Object();
    }

    public Object preloadWebview(Map<String, Object> object) {
        ACTION1<Map<String, Object>> success = (object != null && object.containsKey("success")) ? (ACTION1<Map<String, Object>>) object.get("success") : null;
        ACTION1<Map<String, Object>> fail = (object != null && object.containsKey("fail")) ? (ACTION1<Map<String, Object>>) object.get("fail") : null;
        ACTION1<Map<String, Object>> complete = (object != null && object.containsKey("complete")) ? (ACTION1<Map<String, Object>>) object.get("complete") : null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        }, success, fail, complete);
        return new Object();
    }

    public Object preloadAssets(Map<String, Object> object) {
        ACTION1<Map<String, Object>> success = (object != null && object.containsKey("success")) ? (ACTION1<Map<String, Object>>) object.get("success") : null;
        ACTION1<Map<String, Object>> fail = (object != null && object.containsKey("fail")) ? (ACTION1<Map<String, Object>>) object.get("fail") : null;
        ACTION1<Map<String, Object>> complete = (object != null && object.containsKey("complete")) ? (ACTION1<Map<String, Object>>) object.get("complete") : null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        }, success, fail, complete);
        return new Object();
    }

    public Object getPerformance() {
        return new Object();
    }

    public UserCryptoManager getUserCryptoManager() {
        return new UserCryptoManager();
    }
}

package cn.onekit.js;


import cn.onekit.function;

public class Uint32Array extends TypedArray{
    public final static int BYTES_PER_ELEMENT=4;
    public final static String name = "Uint32Array";

    public <TA extends TypedArray> Uint32Array(Object length) {
        super(Uint32Array.class, length);
    }

    public <TA extends TypedArray> Uint32Array(TA typedArray) {
        super(typedArray);
    }

    public <TA extends TypedArray> Uint32Array(Object buffer, Object byteOffset, Object length) {
        super(Uint32Array.class, buffer, byteOffset, length);
    }

    public <TA extends TypedArray> Uint32Array(Object buffer, Object byteOffset) {
        super(Uint32Array.class, buffer, byteOffset);
    }

    public <TA extends TypedArray> Uint32Array(Array array) {
        super(Uint32Array.class, array);
    }

    //////////////////////////////////
    public static  Uint32Array from(Set source, function mapFn, Object thisArg) {
        return _from(Uint32Array.class, source, mapFn, thisArg);
    }

    public static Uint32Array from(Set source, function mapFn) {
        return from(source, mapFn, null);
    }

    public static Uint32Array from(Set source) {
        return from(source, null);
    }

    //
    public static  Uint32Array from(Array source, function mapFn, Object thisArg) {
        return _from(Uint32Array.class, source, mapFn, thisArg);
    }

    public static Uint32Array from(Array source, function mapFn) {
        return from(source, mapFn, null);
    }

    public static Uint32Array from(Array source) {
        return from(source, null);
    }

    //
    public static  Uint32Array from(Object source, Object mapFn, Object thisArg) {
        return _from(Uint32Array.class, source, mapFn, thisArg);
    }

    public static Uint32Array from(Object source, Object mapFn) {
        return from(source, mapFn, null);
    }

    public static Uint32Array from(Object source) {
        return from(source, null);
    }

    public static Uint32Array of(Object... elements) {
        return _of(Uint32Array.class, elements);
    }


}

package demo.app

import android.content.Context
import cn.onekit.*
import cn.onekit.vue.*
import cn.onekit.weixin2android.*
import cn.onekit.weixin2android.app.*

class `pages$index$index` : WeixinPage() {
    override fun code() {
        Page(hashMapOf<String,Any?>(
            "onShow" to (object: function() { override fun invoke(arguments:Array<out Any?>):Any?{
                wx.reportAnalytics("enter_home_programmatically",hashMapOf<String,Any?>(
                ));
                return null
            }}),"onShareAppMessage" to (object: function() { override fun invoke(arguments:Array<out Any?>):Any?{
                return hashMapOf<String,Any?>(
                    "title" to "小程序官方组件展示","path" to "page/component/index");
            }}),"data" to hashMapOf<String,Any?>(
                "list" to arrayListOf(hashMapOf<String,Any?>(
                    "id" to "view","name" to "视图容器","open" to false,"pages" to arrayListOf("view","scroll-view","swiper","movable-view","cover-view")),hashMapOf<String,Any?>(
                    "id" to "content","name" to "基础内容","open" to false,"pages" to arrayListOf("text","icon","progress","rich-text")),hashMapOf<String,Any?>(
                    "id" to "form","name" to "表单组件","open" to false,"pages" to arrayListOf("button","checkbox","form","input","label","picker","picker-view","radio","slider","switch","textarea","editor")),hashMapOf<String,Any?>(
                    "id" to "nav","name" to "导航","open" to false,"pages" to arrayListOf("navigator")),hashMapOf<String,Any?>(
                    "id" to "media","name" to "媒体组件","open" to false,"pages" to arrayListOf("image","video","camera","live-pusher","live-player")),hashMapOf<String,Any?>(
                    "id" to "map","name" to "地图","open" to false,"pages" to arrayListOf("map")),hashMapOf<String,Any?>(
                    "id" to "canvas","name" to "画布","open" to false,"pages" to arrayListOf("canvas","canvas-2d","webgl")),hashMapOf<String,Any?>(
                    "id" to "open","name" to "开放能力","open" to false,"pages" to arrayListOf("ad","open-data","web-view")),hashMapOf<String,Any?>(
                    "id" to "obstacle-free","name" to "无障碍访问","open" to false,"pages" to arrayListOf("aria-component"))),"theme" to "light"),"onLoad" to (object:
                function() { override fun invoke(arguments:Array<out Any?>):Any?{
                setData(hashMapOf<String,Any?>(
                    "theme" to onekit_or(wx.getSystemInfoSync()["theme"],"light")));
                if(onekit_logic(wx["onThemeChange"])){
                    wx.onThemeChange(object: function() {override fun invoke(arguments: Array<out Any?>): Any? {
                        ;
                        return null
                    }});
                };
                return null
            }}),"kindToggle" to (object: function() { override fun invoke(arguments:Array<out Any?>):Any?{
                val   e = arguments[0];
                val id = e["currentTarget"]["id"];
                val list = this["data"]["list"];
                var i = 0
                var len = list["length"]
                while(i < len){
                    if(onekit_logic(list["i"]["id"] === id)){
                        list["i"]["open"] = !list["i"]["open"];
                    }
                    else {
                        list["i"]["open"] = false;
                    };
                    ++i;};
                setData(hashMapOf<String,Any?>(
                    "list" to list));
                wx.reportAnalytics("click_view_programmatically",hashMapOf<String,Any?>(
                ));
                return null
            }})));


    }

    override fun ui(
        context: Context,
        ui: WeixinElement,
        data: MutableMap<String, Any>?,
        vue: Vue
    ) {

        val ui_0 = View(context)
        ui.addView(ui_0)
        val ui_0_0 = View(context)
        ui_0.addView(ui_0_0)
        if(onekit_logic(data!!["canIUseOpenData"])){
            val ui_0_0_0 = View(context)
            //ui_0_0_0.Tap=""bindViewTap""
            ui_0_0.addView(ui_0_0_0)
            val ui_0_0_0_0 = OpenData(context)
            ui_0_0_0_0.type = "userAvatarUrl"
            ui_0_0_0.addView(ui_0_0_0_0)


            val ui_0_0_1 = OpenData(context)
            ui_0_0_1.type = "userNickName"
            ui_0_0.addView(ui_0_0_1)

        }
        else if(onekit_logic(!data!!["hasUserInfo"])){
            if(onekit_logic(data!!["canIUseGetUserProfile"])){
                val ui_0_0_0 = Button(context)
                //ui_0_0_0.Tap=""getUserProfile""
                ui_0_0.addView(ui_0_0_0)
            }
            else if(onekit_logic(data!!["canIUse"])){
                val ui_0_0_1 = Button(context)
                ui_0_0_1.openType = "getUserInfo"
                //ui_0_0_1.Getuserinfo=""getUserInfo""
                ui_0_0.addView(ui_0_0_1)
            }
            else{
                val ui_0_0_2 = View(context)
                ui_0_0.addView(ui_0_0_2)
            }
        }
        else{
            val ui_0_0_0 = Image(context)
            ui_0_0_0.src = data!!["userInfo"]!!["avatarUrl"]
            ui_0_0_0.mode = "cover"
            //ui_0_0_0.Tap=""bindViewTap""
            ui_0_0.addView(ui_0_0_0)

            val ui_0_0_1 = Text(context)
            ui_0_0.addView(ui_0_0_1)

        }

        val ui_0_1 = View(context)
        ui_0.addView(ui_0_1)
        val ui_0_1_0 = Text(context)
        ui_0_1.addView(ui_0_1_0)






    }
}



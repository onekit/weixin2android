package cn.onekit.weixin2android.app;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

public class SharedElement extends View {
    public SharedElement(Context context) {
        super(context);
    }

    public SharedElement(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onLayout(boolean b, int i, int i1, int i2, int i3) {

    }
}

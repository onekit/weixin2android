package cn.onekit.js;


import cn.onekit.js.core.Onekit_JS;

public class ArrayBuffer  {
    public byte[] _data;
    public ArrayBuffer(byte[] data) {
        _data=data;
    }
    public ArrayBuffer(Object data) {
        if (data instanceof Array) {
            Array array = (Array) data;
            this._data = new byte[array.size()];
            for (int i=0;i<array.size();i++) {
                byte item = (byte) array.get(i);
                this._data[i] =item;
            }
        } else if (data instanceof java.lang.Number) {
            _data = new byte[(int) data];
        }
    }

    public String ToString() {
        return new String(String.format("ArrayBuffer { byteLength: %d }", getByteLength()));
    }

    ////////////////////////////////////
    public int getByteLength() {
        return _data.length;
    }

    public byte get(int index) {
        return _data[index];
    }

    public static boolean isView(Object arg) {
        if (arg == null) {
            return false;
        }
        return (arg instanceof TypedArray) || (arg instanceof DataView);
    }

    public static boolean isView() {
        return false;
    }

    public ArrayBuffer slice(Object begin, Object end) {
        int b = Onekit_JS.number(begin, 0, 0).intValue();
        int e = Onekit_JS.number(end, 0, 0).intValue();
        byte[] data = new byte[e - b];
        System.arraycopy(this._data, b, data, 0, e - b);
        ArrayBuffer result = new ArrayBuffer((int)(e - b));
        result._data = data;
        return result;
    }

    public ArrayBuffer slice(int begin) {
        return slice(begin, getByteLength());
    }

    public static ArrayBuffer transfer(ArrayBuffer oldBuffer, Object newByteLength) {
        ArrayBuffer result = new ArrayBuffer(newByteLength);
        int len = ((int) newByteLength);
        for (int i = 0; i < len && i < oldBuffer._data.length; i++) {
            result._data[i] = oldBuffer._data[i];
        }

        oldBuffer._data = new byte[]{};
        return result;
    }

    public static ArrayBuffer transfer(ArrayBuffer oldBuffer) {
        return transfer(oldBuffer, oldBuffer.getByteLength());
    }

}

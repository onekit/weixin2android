package cn.onekit.w3c.core;

public interface FormItem_<T extends Object> {

    void setName(String name);

    String getName();

    //
    void setValue(T value);

    T getValue();

    ///////////////////////////
    public abstract void reset();
}

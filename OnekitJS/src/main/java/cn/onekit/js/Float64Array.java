package cn.onekit.js;


import cn.onekit.function;

public class Float64Array extends TypedArray<Double> {
    public final static int BYTES_PER_ELEMENT=8;
    public final static String name="Float64Array";
    public Float64Array(Object length) {
        super(Float64Array.class,length);
    }

    public Float64Array(TypedArray<Double> typedArray) {
        super(typedArray);
    }

    public Float64Array(Object buffer, Object byteOffset, Object length) {
        super(Float64Array.class,buffer, byteOffset, length);
    }

    public Float64Array(Object buffer, Object byteOffset) {
        super(Float64Array.class,buffer, byteOffset);
    }


    public Float64Array(Array array) {
        super(Float64Array.class,array);
    }
    //////////////////////////////////
    public static  Float64Array from(Set source, function mapFn, Object thisArg) {
        return _from(Float64Array.class, source, mapFn, thisArg);
    }
    public static Float64Array of(Object... elements) {
        return _of(Float64Array.class, elements);
    }
}

package cn.onekit.weixin2android.app;

import java.util.HashMap;

import cn.onekit.weixin2android.app.core.WeixinFile;

public  abstract class WeixinModule implements WeixinFile {
    public abstract void onekit();

    public  java.util.Map<String, Object> module = new HashMap<String, Object>();
}

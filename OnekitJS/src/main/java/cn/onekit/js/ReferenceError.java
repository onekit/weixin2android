package cn.onekit.js;



public class ReferenceError extends Error {
    public ReferenceError(Object message) {
        super(message);
    }
    public ReferenceError() {
        super();
    }
}

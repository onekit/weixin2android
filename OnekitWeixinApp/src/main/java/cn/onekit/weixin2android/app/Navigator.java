package cn.onekit.weixin2android.app;

import android.content.Context;
import android.util.AttributeSet;

import java.util.HashMap;
import cn.onekit.weixin2android.app.WeixinElement;

public class Navigator extends WeixinElement {
    public Navigator(Context context) {
        super(context);
        _init();
    }

    public Navigator(Context context, AttributeSet attrs) {
        super(context, attrs);
        _init();
    }

    void _init() {
        this.setOnClickListener(v -> {
            wx.navigateTo(new HashMap<String,Object>() {{
                put("url", getUrl());
            }});
        });
    }

    private String getUrl() {
        return url;
    }

    String url;
    public void setUrl(String url) {
        this.url = url;
    }

    String openType;
    public void setOpenType(String openType) {
        this.openType=openType;
    }

}

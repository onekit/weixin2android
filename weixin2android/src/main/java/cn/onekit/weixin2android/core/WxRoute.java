package cn.onekit.weixin2android.core;

import java.util.Map;

import cn.onekit.ACTION1;
import cn.onekit.FUNC2;

public abstract class WxRoute extends WxBase {

public Object switchTab(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object reLaunch(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object redirectTo(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object navigateTo(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object navigateBack(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
}

package cn.onekit.js;


import cn.onekit.function;

public class Float32Array extends TypedArray<Double> {
    public final static int BYTES_PER_ELEMENT=4;
    public final static String name="Float32Array";
    public Float32Array(Object length) {
        super(Float32Array.class,length);
    }

    public Float32Array(TypedArray<Double> typedArray) {
        super(typedArray);
    }

    public Float32Array(Object buffer, Object byteOffset, Object length) {
        super(Float32Array.class,buffer, byteOffset, length);
    }

    public Float32Array(Object buffer, Object byteOffset) {
        super(Float32Array.class,buffer, byteOffset);
    }


    public Float32Array(Array array) {
        super(Float32Array.class,array);
    }
    //////////////////////////////////
    public static  Float32Array from(Set source, function mapFn, Object thisArg) {
        return _from(Float32Array.class, source, mapFn, thisArg);
    }
    public static Float32Array of(Object... elements) {
        return _of(Float32Array.class, elements);
    }
}

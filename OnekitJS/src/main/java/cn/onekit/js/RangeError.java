package cn.onekit.js;



public class RangeError extends Error {
    public RangeError(Object message) {
        super(message);
    }
    public RangeError() {
        super();
    }
}
package cn.onekit.weixin2android;

import java.util.HashMap;
import java.util.Map;

import cn.onekit.function;


public class EventChannel extends HashMap<String,Object> {
    public static Map<Integer,EventChannel> eventChannels=new HashMap();
    /////////////////////
    Map<String, function> onHandlers = new HashMap();
    Map<String, function> onceHandlers = new HashMap();
    public EventChannel(int channelID,int otherID){
        this.otherID=otherID;
        eventChannels.put(channelID,this);
    }
    private final int otherID;
    public void emit(String eventName, Object args) {
        EventChannel other = eventChannels.get(otherID);
        if (other.onHandlers.containsKey(eventName)) {
            other.onHandlers.get(eventName).invoke(args);
        }else  if (other.onceHandlers.containsKey(eventName)) {
            other.onceHandlers.get(eventName).invoke(args);
            other.onceHandlers.remove(eventName);
        }
    }

    public void on(String eventName, function fn) {
        onHandlers.put(eventName, fn);
    }

    public void once(String eventName, function fn) {
        onceHandlers.put(eventName, fn);
    }

    public void off(String eventName, function fn) {
        onHandlers.remove(eventName);
    }

}

package cn.onekit;

public interface ACTION3<T1,T2,T3> {

void invoke(final T1 arg1, final T2 arg2, final T3 arg3);
}

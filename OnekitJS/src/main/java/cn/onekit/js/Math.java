package cn.onekit.js;


import cn.onekit.js.core.Onekit_JS;

public class Math  {
    public static final double E = java.lang.Math.E;
    public static final double LN10 =  2.302585092994046;
    public static final double LN2 =  0.6931471805599453;
    public static final double LOG10E =  0.4342944819032518;
    public static final double LOG2E =  1.4426950408889634;
    public static final double PI =  java.lang.Math.PI;
    public static final double SQRT1_2 =  0.7071067811865476;
    public static final double SQRT2 =  1.4142135623730951;

    public static double abs(Object x) {
        if (x == null) {
            return  0;
        }
        if (!Onekit_JS.isNumber(x)) {
            return Number.NaN;
        }
        Double result = java.lang.Math.abs(Onekit_JS.number(x,0,0).doubleValue());
        return  Onekit_JS.float2integer(result);
    }

    public static double abs() {
        return abs(0);
    }

    public static double acos(Object v) {
        double x = Onekit_JS.number(v,0,0).doubleValue();
        if (x < -1 || x > 1) {
            return Number.NaN;
        }
        return java.lang.Math.acos(Onekit_JS.number(v,0,0).doubleValue());
    }

    public static double acosh(Object v) {
        double x = Onekit_JS.number(v,0,0).doubleValue();
        if (x < 1) {
            return Number.NaN;
        }
        return java.lang.Math.log(x + java.lang.Math.sqrt(x * x - 1));
    }

    public static double asin(Object v) {
        double x = Onekit_JS.number(v,0,0).doubleValue();
        if (x < -1 || x > 1) {
            return Number.NaN;
        }
        return  java.lang.Math.asin(x);
    }

    public static double asinh(Object v) {
        Double x = Onekit_JS.number(v,0,0).doubleValue();
        return java.lang.Math.log(x + java.lang.Math.sqrt(x * x + 1));
    }

    public static double atan(Object v) {
        double x = Onekit_JS.number(v,0,0).doubleValue();
        return java.lang.Math.atan(x);
    }

    public static double atan2(Object a, Object b) {
        double y = Onekit_JS.number(a,0,0).doubleValue();
        double x = Onekit_JS.number(b,0,0).doubleValue();
        return java.lang.Math.atan2(y, x);
    }

    public static double atanh(Object v) {
        double x = Onekit_JS.number(v,0,0).doubleValue();
        if (x < -1 || x > 1) {
            return Number.NaN;
        }
        return java.lang.Math.log((1 + x) / (1 - x) / 2);
    }

    public static double cbrt(Object v) {
        if (v == null) {
            return 0;
        }   
        if (!Onekit_JS.isNumber(v)) {
            return Number.NaN;
        }
        double x = Onekit_JS.number(v,0,0).doubleValue();
        double number = Double.parseDouble(String.valueOf(x));
        double result = java.lang.Math.cbrt(number);
        return Onekit_JS.float2integer(result);
    }

    public static double ceil(Object v) {
        double x = Onekit_JS.number(v,0,0).doubleValue();
        return  java.lang.Math.ceil(x);
    }

    public static double clz32(Object v) {
        if (v == null) {
            return 32;
        }
        if (v instanceof Boolean) {
            return 31;
        }
        double db = Onekit_JS.number(v,0,0).doubleValue();
        long l = Double.doubleToLongBits(db);
        Double value = Double.longBitsToDouble(l);
        return value != 0 ? 32 - value.toString().length() : 32;
    }

    public static double clz32() {
        return clz32(null);
    }

    public static double cos(Object v) {
        double x = Onekit_JS.number(v,0,0).doubleValue();
        return java.lang.Math.cos(x);
    }

    public static double cosh(Object v) {
        double x = Onekit_JS.number(v,0,0).doubleValue();
        double y = java.lang.Math.exp(x);
        return (y + 1 / y) / 2;
    }

    public static double exp(Object v) {
        double x = Onekit_JS.number(v,0,0).doubleValue();
        return java.lang.Math.exp(x);
    }

    public static double expm1(Object x) {
        if (x == null) {
            return 0;
        }
        if (!Onekit_JS.isNumber(x)) {
            return Number.NaN;
        }
        double db = Onekit_JS.number(x,0,0).doubleValue();
        return java.lang.Math.exp(db) - 1;
    }

    public static double floor(Object v) {
        double x = Onekit_JS.number(v,0,0).doubleValue();
        return (long) java.lang.Math.floor(x);
    }

    public static double fround(Object doubleFloat) {
        if (doubleFloat == null) {
            return 0;
        }
        if (!Onekit_JS.isNumber(doubleFloat)) {
            return Number.NaN;
        }
        return  Onekit_JS.number(doubleFloat,0,0).floatValue();
    }

    public static double hypot(Object... arguments) {
        if (arguments == null) {
            return  0;
        }
        double y = 0;

        for (Object argument : arguments) {
            if (!Onekit_JS.isNumber(argument)) {
                return Number.NaN;
            }
            double number = Onekit_JS.number(argument,0,0).doubleValue();
            if (number == Number.POSITIVE_INFINITY || number == Number.NEGATIVE_INFINITY) {
                return Number.POSITIVE_INFINITY;
            }
            y += number * number;
        }
        return  java.lang.Math.sqrt(y);
    }

    public static double imul(Object x, Object y) {
        int a = Onekit_JS.number(x,0,0).intValue();
        int b = Onekit_JS.number(y,0,0).intValue();
        a |= 0;
        long result = (a & 0x003fffff) * b;
        if ((a & 0xffc00000) != 0) result += (a & 0xffc00000) * b;
        return result;
    }

    public static double log(Object v) {
        double x = Onekit_JS.number(v,0,0).doubleValue();
        return java.lang.Math.log(x);
    }

    public static double log10(Object v) {
        double x = Onekit_JS.number(v,0,0).doubleValue();
        if (!Onekit_JS.isNumber(v)) {
            return Number.NaN;
        }
        return java.lang.Math.log10(x);
    }

    public static double log1p(Object v) {
        double number = Onekit_JS.number(v,0,0).doubleValue();
        return java.lang.Math.log1p(number);
    }

    public static double log2(Object v) {
        double number = Onekit_JS.number(v,0,0).doubleValue();
        return number / Math.log(2);
    }
    public static double max(Object... numbers) {
            double result = Onekit_JS.number(numbers[0],0,0).doubleValue();
            for(int i=1;i<numbers.length;i++){
                double element = Onekit_JS.number(numbers[i],0,0).doubleValue();
               if(element>result){
                   result= element;
               }

            }
            return result;
        }

    public static double min(Object... numbers) {
        double result = Onekit_JS.number(numbers[0],0,0).doubleValue();
        for(int i=1;i<numbers.length;i++){
            double element = Onekit_JS.number(numbers[i],0,0).doubleValue();
            if(element<result){
                result= element;
            }

        }
        return  result;
    }

    public static double pow(Object a, Object b) {
        double x = Onekit_JS.number(a,0,0).doubleValue();
        double y = Onekit_JS.number(b,0,0).doubleValue();
        return java.lang.Math.pow(x, y);
    }

    public static double random() {
        return java.lang.Math.random();
    }

    public static double round(Object v) {
        double x = Onekit_JS.number(v,0,0).doubleValue();
        return java.lang.Math.round(x);
    }

    public static double sign(Object v) {
        double number = Onekit_JS.number(v,0,0).doubleValue();
        if (number < 0) {
            return -1;
        } else if (number > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public static double sign() {
        return sign(null);
    }

    public static double sin(Object v) {
        double x = Onekit_JS.number(v,0,0).doubleValue();
        return java.lang.Math.sin(x);
    }

    public static double sinh(Object v) {
        if (v == null) {
            return Number.NaN;
        }
        double number = Onekit_JS.number(v,0,0).doubleValue();
        return (java.lang.Math.exp(number)- java.lang.Math.exp(-number)) / 2;
    }

    public static double sqrt(Object v) {
        if (v == null) {
            return Number.NaN;
        }
        double number = Onekit_JS.number(v,0,0).doubleValue();
        return java.lang.Math.sqrt(number);
    }

    public static double tan(Object v) {
        if (v == null) {
            return Number.NaN;
        }
        double number = Onekit_JS.number(v,0,0).doubleValue();
        return java.lang.Math.tan(number);
    }

    public static String toSource() {
        return new String("Math");
    }
    public static double tanh(Object v) {
        if (v == null) {
            return Number.NaN;
        }
        double number = Onekit_JS.number(v,0,0).doubleValue();
        if (number == Number.POSITIVE_INFINITY) {
            return 1;
        } else if (number == Number.NEGATIVE_INFINITY) {
            return -1;
        } else {
            double y = java.lang.Math.exp(2 * number);
            return (y - 1) / (y + 1);
        }
    }

    public static double trunc(Object v) {
        if (v == null) {
            return Number.NaN;
        }
        double x = Onekit_JS.number(v,0,0).doubleValue();
        String string = String.valueOf(x);
        int dot = string.indexOf(".");
        if (dot>=0 ) {
            string = string.substring(0,dot);
        }
        return Double.parseDouble(string);
    }
    public static double trunc() {
        return trunc(null);
    }

    
}

package cn.onekit.weixin2android.core.nfc;

import android.nfc.cardemulation.HostApduService;
import android.os.Bundle;
import android.util.Log;

import java.util.Arrays;

import cn.onekit.function;
import cn.onekit.js.ArrayBuffer;
import java.util.HashMap;

public class NfcService extends HostApduService {
    public static function callback;
    public static byte[] data;
    @Override
    public byte[] processCommandApdu(byte[] commandApdu, Bundle extras) {
        if(callback==null){
            return null;
        }
        callback.invoke(new HashMap<String,Object>());//1,new ArrayBuffer(commandApdu),1));
        return data;
    }

    @Override
    public void onDeactivated(int reason) {

    }
}

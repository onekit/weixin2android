package cn.onekit.weixin2android.app;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import cn.onekit.Android;
import cn.onekit.TheKit;
import cn.onekit.css.core.OnekitCSS;
import cn.onekit.weixin2android.WX;
import cn.onekit.weixin2android.app.core.WeixinFile;

public abstract class WeixinApplication extends Application implements WeixinFile,Map<String,Object> {

    public WeixinApplication(){
        super();
    }
    public Map<String,Object> THIS;

    protected void App(Map<String,Object> OBJECT) {
        THIS = OBJECT;
    }

    protected WX wx ;
    @Override
    public void onCreate() {
        super.onCreate();
        Android.context = this;
        TheKit.currentUrl = "/";
        OnekitCSS.share = new OnekitCSS(Android.SIZE(),"miniprogram/");
        OnekitCSS.share.init(new String[]{"/onekit.wxss","/app.wxss"});
         wx = new WX();
        onekit_js();
    }

   protected abstract   void onekit_js();


    @Override
    public int size() {
        return THIS.size();
    }

    @Override
    public boolean isEmpty() {
        return THIS.size()==0;
    }

    @Override
    public boolean containsKey(@Nullable Object o) {
        return THIS.containsKey(o);
    }

    @Override
    public boolean containsValue(@Nullable Object o) {
        return THIS.containsValue(o);
    }

    @Nullable
    @Override
    public Object get(@Nullable Object o) {
        return THIS.get(o);
    }

    @Nullable
    @Override
    public Object put(String s, Object o) {
        return THIS.put(s,o);
    }

    @Nullable
    @Override
    public Object remove(@Nullable Object o) {
        return THIS.remove(o);
    }

    @Override
    public void putAll(@NonNull Map<? extends String, ?> map) {
        THIS.putAll(map);
    }

    @Override
    public void clear() {
        THIS.clear();
    }

    @NonNull
    @Override
    public Set<String> keySet() {
        return THIS.keySet();
    }

    @NonNull
    @Override
    public Collection<Object> values() {
        return THIS.values();
    }

    @NonNull
    @Override
    public Set<Entry<String, Object>> entrySet() {
        return THIS.entrySet();
    }
}

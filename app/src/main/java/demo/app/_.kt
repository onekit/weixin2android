package demo.app

operator fun Any?.not(): Any {
    if(this==null){
        return true
    }
    return !this
}


 operator fun Any?.compareTo(right: Any?): Int {
    return this!!.compareTo(right!!);
}

 operator fun Any?.get(key: Any?): Any? {
    if(this is Map<*, *>){
        return this[key];
    }else   if(this is Array<*>){
        return this[key!! as Int];
    }else{
        val clazz = this!!.javaClass;
        for(method in   clazz.methods){
            if(method.name==key){
                return method;
            }
        }
        for(field in   clazz.fields){
            if(field.name==key){
                return field.get(this);
            }
        }
        return null;
    }
}

 operator fun Any?.set(key: Any?, value: Any) {
     if(this is Map<*, *>){
          this[key] = value;
     }else   if(this is Array<*>){
          this[key!! as Int] = value;
     }else{
         val clazz = this!!.javaClass;
         for(method in   clazz.methods){
             if(method.name==key){
                  method.invoke(this,value);
             }
         }
         for(field in   clazz.fields){
             if(field.name==key){
                  field.set(this,value);
             }
         }
     }
}
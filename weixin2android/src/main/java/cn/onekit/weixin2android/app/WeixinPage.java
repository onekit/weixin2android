package cn.onekit.weixin2android.app;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.onekit.Android;
import cn.onekit.TheKit;
import cn.onekit.css.core.CssLayoutParams;
import cn.onekit.js.function;
import cn.onekit.vue.Vue;
import cn.onekit.weixin2android.EventChannel;
import cn.onekit.weixin2android.PageObject;
import cn.onekit.weixin2android.app.core.WeixinFile;


public  abstract class WeixinPage extends AppCompatActivity implements WeixinFile {
    public void setData(Map<String, Object> data) {

    }

    private void update() {
        //LOG LOG = new LOG();
        page.removeAllViews();
        //LOG.add("removeAllViews");
        Map<String, Object> data = (Map<String, Object>) object.get("data");
        ui(this, page, data, new Vue(data));
        //LOG.add("ui");
//        OnekitCSS.share.run(page, new String[]{"/" + url + ".wxss"});
        //LOG.add("OnekitCSS.share.run");
    }

    //////////////////////////////////
    Map<String, Class<WeixinTemplate>> imports = new HashMap();

    public void Import(String src) {
        src = src.substring(0, src.lastIndexOf("/");
        src = TheKit.fixPath(url, src);
        src = TheKit.url2class(this, src);
        try {
            Class classes = Class.forName(src);
            for (Class<WeixinTemplate> clazz : classes.getClasses()) {
                imports.put(clazz.getSimpleName(), clazz);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void Template(String is, Map<String, Object> data, ViewGroup parent) {
        try {
            WeixinTemplate template = imports.get(is).getConstructor(Context.class).newInstance(this);
            template.ui(parent, data, new Vue(data));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        ;
    }

    protected Map<String, Object> object = new HashMap<String, Object>();
    //
    static Map<Integer, EventChannel> allEventChannel = new HashMap();

    public abstract void ui(  Context       context,final WeixinElement ui, final Map<String,Object> data, final Vue vue);

    protected Page page;

    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().getDecorView().setBackgroundColor(Color.WHITE);
        Android.context = this;
        ////////////////////////
        page = new Page(this);
        setContentView(page);
        page.setLayoutParams(new CssLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        Bundle extras = getIntent().getExtras();
        TheKit.currentUrl = url = TheKit.class2url(this, this.getClass().getName(), extras);
        pages.add(new PageObject(url));
        code();
        update();
    }

    List<PageObject> pages = new ArrayList<>();

    @Override
    protected void onStart() {
        super.onStart();
        if (object == null) {
            console.error(new String(String.format("Page \"%s\" has not been registered yet.", url)));
        }
        if (object.containsKey("onReady")) {
            ((function) object.get("onReady")).invoke();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        TheKit.currentUrl = url;
        if (object.containsKey("onShow")) {
            ((function) object.get("onShow")).invoke();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (object.containsKey("onHide")) {
            ((function) object.get("onHide")).invoke();
        }
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode>0){
//            setResult(--resultCode);
//            finish();
//        }
//    }

    protected void Page(Map<String, Object> object) {
        this.object = object;
        if (!object.containsKey("data")) {
            object.put("data", new HashMap<String, Object>());
        }
        /*object.set("setData",new function(){
            @Override
            public Object invoke(Object... arguments) {
                Map<String,Object> data = (Map<String,Object>) arguments[0];
                for (String key : data.keySet()) {
                    object.put(key, data.get(key));
                }
                ((WeixinPage) Android.context).update();
                return null;
            }
        });*/
        object.put("getOpenerEventChannel", new cn.onekit.js.function() {
            @Override
            public Object invoke(Object... arguments) {

                int channelID = getIntent().getIntExtra("onekit_channelID", 0);
                return EventChannel.eventChannels.get(channelID);
            }

        });
        if (object.containsKey("onLoad")) {
            Bundle extras = getIntent().getExtras();
            Map<String, Object> query = new HashMap<String, Object>();
            if (extras != null) {
                for (String key : extras.keySet()) {
                    if (key.startsWith("onekit_")) {
                        continue;
                    }
                    query.put(key, new String(extras.getString(key)));
                }
            }
            ((function) object.get("onLoad")).invoke(query);
        }
    }

    public static final int VideoView = 1;
    public static final int IMAGE_CAPTURE = 100;
    public static final int IMAGE_CAPTURE_PATH = 101;
    public static final int VIDEO_CAPTURE = 200;
    public static final int GET_CONTENT = 300;
    public static final int GET_CONTENT_PATH = 301;
    public static final int GET_MINI = 400;
    public static final int GET_URI = 500;
    public static final int SCANNIN_GREQUEST_CODE = 202;
    public static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 203;
    public static final int location = 204;
    public static final int chooseLocation = 205;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case chooseLocation: {
                this.wx._onChooseLocation(resultCode, data);
            }
            break;
            case location: {
                this.wx._onOpenlocation(resultCode, data);
            }
            break;
//            case SCANNIN_GREQUEST_CODE: {
//                ScanCode.SCANNIN_GREQUEST_CODE(resultCode, data);
//            }
//            break;
//            case CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE: {
//                Video.CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE(resultCode, data);
//            }
//            break;
//            case VideoView: {
//                Video.VideoView(resultCode, data);
//            }
//            break;
            default:
                break;
        }
    }

    protected abstract void code();
}

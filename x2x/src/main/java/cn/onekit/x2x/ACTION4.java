package cn.onekit;

public interface ACTION4<T1,T2,T3,T4>  {

   void invoke(final T1 arg1, T2 arg2,T3 arg3,T4 arg4);
}

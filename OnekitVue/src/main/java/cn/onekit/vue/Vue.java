package cn.onekit.vue;

import java.util.Map;

import cn.onekit.ACTION1;

public class Vue {
    Object data;

    public Vue(Object data) {
        this.data = data;
    }

    public void For(Object forItems, String forKey, String forIndex, String forItem, ACTION1 callback) {
        Iterable<Object> items = (Iterable) forItems;
        if (items == null) {
            return;
        }
        int index = 0;
        Map<String,Object> DATA = (Map<String,Object>) data;
        for (Object item : items) {
            DATA.put(forItem, item);
            DATA.put(forIndex,index);
            callback.invoke(DATA);
            index++;
        }
    }
}

package cn.onekit.weixin2android.core.wx.canvas;

import cn.onekit.weixin2android.core.wx.analysis.WxAnalysis;

public abstract class WxCanvas extends WxAnalysis {
    public Object createOffscreenCanvas(Object object, double width, double height, Object This) {
           return new Object();
    }
    public Object createCanvasContext(String canvasId ,  Object This) {
            return new Object();
    }
    public Object canvasToTempFilePath(String object ,  Object This) {
            return new Object();
    }
    public Object canvasPutImageData(String object ,  Object This) {
             return new Object();
    }
    public Object canvasGetImageData(String object ,  Object This) {
           return new Object();
    }
}

package cn.onekit.js;

import cn.onekit.function;
import cn.onekit.function;
import cn.onekit.js.core.*;

public class Promise  {
    function _callback;

    @Override
    public String toString() {
        return "[object Promise]";
    }

    ///////////////////////////////
    public Promise(function callback) {
        _callback = callback;
    }

    //////////////////////////////////
    public static Promise all(Array iterable) {
        return null;
        /*return new Promise((resolve, reject) -> {
            Array result = new Array();
            for (Object item : iterable) {
                if (item instanceof Promise) {
                    Promise promise = (Promise) item;
                    //promise.then(new cn.onekit.function() {
                   //     @Override
                   //     public Object invoke(Object... arguments) {
                  //          Object arg=arguments[0];
                  //           result.add(arg);
                 //            return null;
                 //       }
                //    });
                } else {
                    result.add(item);
                }
            }
      // reject).invoke(result);
            return null;
        });*/
    }

    public static Promise all(String iterable) {
        return null;
    }

    public static Promise allSettled(Array iterable) {
        return null;
    }

    public static Promise allSettled(String iterable) {
        return null;
    }

    public Promise Catch(function onRejected) {
        return null;
    }

    public Promise Finally(function onFinally) {
        return null;
    }

    public Promise then(cn.onekit.function onFulfilled, cn.onekit.function onRejected) {
        _callback.invoke(onFulfilled, onRejected);
        return this;
    }

    public Promise then(cn.onekit.function onFulfilled) {
        return then(onFulfilled, null);
    }

    public static Promise race(Array iterable) {
        return null;
    }

    public static Promise race(String iterable) {
        return null;
    }

    public static Promise reject(Error reason) {
        return null;//new Promise((resolve, reject) -> (()invoke(reason));
    }

    public static Promise resolve(Object value) {
        return new Promise(new function(){

            @Override
            public Object invoke(Object... arguments) {
                try {
                    function resolve = (function) arguments[0];
                    resolve.invoke(value);
                }catch (Exception e){
                    e.printStackTrace();
                    function reject = (function) arguments[1];
                    reject.invoke(value);
                }
                return null;
            }
        });
    }


    
}

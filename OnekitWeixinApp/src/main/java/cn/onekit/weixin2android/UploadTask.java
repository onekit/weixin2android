package cn.onekit.weixin2android;

import cn.onekit.function;
import okhttp3.Call;
import okhttp3.OkHttpClient;

/**
 * Created by Administrator on 2017/12/25 0025.
 */

public class UploadTask {
    OkHttpClient okHttpClient;
    public Call call;
    public function onProgressUpdate;
    public void onProgressUpdate(function callback){
        onProgressUpdate = callback;
    }
    public void abort(){
        call.cancel();
    }
}

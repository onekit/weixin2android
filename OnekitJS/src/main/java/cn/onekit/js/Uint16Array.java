package cn.onekit.js;


import cn.onekit.function;

public class Uint16Array extends TypedArray<Integer>{
    public final static int BYTES_PER_ELEMENT=2;
    public final static String name = "Uint16Array";
    public Uint16Array(Object length) {
        super(Uint16Array.class,length);
    }


    public Uint16Array(Object buffer, Object byteOffset, Object length) {
        super(Uint16Array.class,buffer, byteOffset, length);
    }

    public Uint16Array(Object buffer, Object byteOffset) {
        super(Uint16Array.class,buffer, byteOffset);
    }


    public Uint16Array(Array array) {
        super(Uint16Array.class,array);
    }
    //////////////////////////////////
    public static  Uint16Array from(Set source, function mapFn, Object thisArg) {
        return _from(Uint16Array.class, source, mapFn, thisArg);
    }

    public static Uint16Array from(Set source, function mapFn) {
        return from(source, mapFn, null);
    }

    public static Uint16Array from(Set source) {
        return from(source, null);
    }

    //
    public static  Uint16Array from(Array source, function mapFn, Object thisArg) {
        return _from(Uint16Array.class, source, mapFn, thisArg);
    }

    public static Uint16Array from(Array source, function mapFn) {
        return from(source, mapFn, null);
    }

    public static Uint16Array from(Array source) {
        return from(source, null);
    }

    //
    public static  Uint16Array from(Object source, Object mapFn, Object thisArg) {
        return _from(Uint16Array.class, source, mapFn, thisArg);
    }

    public static Uint16Array from(Object source, Object mapFn) {
        return from(source, mapFn, null);
    }

    public static Uint16Array from(Object source) {
        return from(source, null);
    }

    public static Uint16Array of(Object... elements) {
        return _of(Uint16Array.class, elements);
    }
}

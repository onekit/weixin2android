package cn.onekit.js;

import android.util.Log;

import java.util.Random;

import cn.onekit.js.core.Onekit_JS;


public class Console  {
    String _run(Object... data) {
        StringBuilder str = new StringBuilder(" \r\n");
        for (int i=0;i<data.length;i++) {
            Object item = data[i];
            str.append(String.format("%s\t", Onekit_JS.toString(item)));
        }
        return str.toString();
    }

    public void asset(Object... assets) {

        Log.v("[OneKit]====================="+ new Random().nextInt(), _run(assets));
    }

    public void error(Object... errors) {

        Log.e("[OneKit]====================="+ new Random().nextInt(), _run(errors));
    }

    public void info(Object... infos) {

        Log.i("[OneKit]====================="+new Random().nextInt(), _run(infos));
    }

    public void log(Object... logs) {

        Log.d("[OneKit]====================="+ new Random().nextInt(), _run(logs));
    }

    public void warn(Object... warns) {

        Log.w("[OneKit]====================="+ new Random().nextInt(), _run(warns));
    }


}

package cn.onekit.weixin2android;

public class UserInfo {
    String nickName;
    String avatarUrl;
    String gender;
    String city;
    String province;
    String country;
    String language;
}

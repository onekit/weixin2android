package cn.onekit.weixin2android.core.wx.basic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.onekit.ACTION1;
import cn.onekit.FUNC2;
import cn.onekit.function;
import cn.onekit.js.ArrayBuffer;
import cn.onekit.weixin2android.RealtimeLogManager;
import cn.onekit.weixin2android.UserCryptoManager;
import cn.onekit.weixin2android.core.wx.Wx;

public abstract class WxPerformance extends WxMiniprogram {

    public Object reportPerformance(Number id, Number value, String dimensions) {
        return new Object();
    }

    public Object reportPerformance(Number id, Number value, List<Object> dimensions) {
        return new Object();
    }

    public Object preloadWebview(Map<String, Object> object) {
        ACTION1<Map<String, Object>> success = (object != null && object.containsKey("success")) ? (ACTION1<Map<String, Object>>) object.get("success") : null;
        ACTION1<Map<String, Object>> fail = (object != null && object.containsKey("fail")) ? (ACTION1<Map<String, Object>>) object.get("fail") : null;
        ACTION1<Map<String, Object>> complete = (object != null && object.containsKey("complete")) ? (ACTION1<Map<String, Object>>) object.get("complete") : null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        }, success, fail, complete);
        return new Object();
    }

    public Object preloadAssets(Map<String, Object> object) {
        ACTION1<Map<String, Object>> success = (object != null && object.containsKey("success")) ? (ACTION1<Map<String, Object>>) object.get("success") : null;
        ACTION1<Map<String, Object>> fail = (object != null && object.containsKey("fail")) ? (ACTION1<Map<String, Object>>) object.get("fail") : null;
        ACTION1<Map<String, Object>> complete = (object != null && object.containsKey("complete")) ? (ACTION1<Map<String, Object>>) object.get("complete") : null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        }, success, fail, complete);
        return new Object();
    }

    public Object getPerformance() {
        return new Object();
    }
}

package cn.onekit.js;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import cn.onekit.function;
import cn.onekit.js.core.Onekit_JS;


public class Array extends ArrayList<Object>  {

    public Array(List<Object> subList) {
        super(subList);
    }

    /////////////////////////////////////////
    public int getLength() {
        return this.size();
    }

    private void _setLength(long length) {
        if (length < 0 || length >= 4294967296L) {
            throw new RangeError(new String("Invalid array length"));
        }
        if (length > this.size()) {
            return;
        }
        for (long i = length; i <= this.size(); i++) {
            this.remove(this.size() - 1);
        }
    }

    public void setLength(Object length) {
        int length_ = Onekit_JS.number(length, 0, 0).intValue();
        _setLength(length_);
    }

    int _hashCode = new Random().nextInt();

    @Override
    public int hashCode() {
        return _hashCode;
    }

    private static int _index(Array array, int index) {
        if (index >= 0) {
            return index;
        }
        return array.size() + index;
    }


    

    //////////////////////////////////////
    public static Array from(Object arrayLike, Object mapFn, Object thisArg) {
        if (arrayLike instanceof Array) {
            return from((Array) arrayLike, mapFn, thisArg);
        } else if (arrayLike instanceof String) {
            return from(((String) arrayLike), mapFn, thisArg);
        } else {
            return null;
        }
    }

    private static Array from(Array array, Object mapFn, Object thisArg) {
        Array result = new Array();
        for (Object element : array) {
            if (mapFn != null) {
                result.add(((function)mapFn).invoke(element));
            } else {
                result.add(element);
            }
        }
        return result;
    }

    private static Array from(String arrayLike, Object mapFn, Object thisArg) {
        return from(Onekit_JS.string2Array(arrayLike), mapFn, thisArg);
    }


    //
    public static boolean isArray(Object obj) {
        return obj instanceof Array;
    }

    //
    public static Array of(Object... elements) {
        Array result = new Array();
        Collections.addAll(result, elements);
        return result;
    }

    /////////////////////////////////////////
    public Array() {
    }

    public Array(Object length) {
        if (length == null) {
            add(null);
            return;
        }
        if (Onekit_JS.isNumber(length)) {
            long length_ = Long.parseLong( length.toString());
            if (length_ <= 0 || length_ >= 4294967296L) {
                throw new Error(new String("Invalid array length"));
            }
            for (int i = 0; i < length_; i++) {
                this.add(null);
            }
        } else {
            add(length);
        }
    }

    public Array(Object... args) {

        Collections.addAll(this, args);

    }
    /////////////////////////////////////////////////

    public Array concat(Object... values) {
        Array RESULT = (Array) this.clone();
        for (Object value : values) {
            if (value instanceof Array) {
                Array array = (Array) value;
                RESULT.addAll(array);
            } else {
                RESULT.add(value);
            }
        }
        return RESULT;
    }

    //
    private Array _copyWithin(int target, int start, int end) {
        target = _index(this, target);
        start = _index(this, start);
        end = _index(this, end);
        Array result = new Array();
        result.addAll(this);
        for (int i = start, j = 0; i < end && i < result.size() && target + j < result.size(); i++, j++) {
            result.set(target + j, this.get(i));
        }
        return result;
    }

    public Array copyWithin(int target, int start, int end) {
        target = _index(this, target);
        start = _index(this, start);
        end = _index(this, end);
        Array result = new Array();
        result.addAll(this);
        for (int i = start, j = 0; i < end && i < result.size() && target + j < result.size(); i++, j++) {
            result.set(target + j, this.get(i));
        }
        return result;
    }

    public Array copyWithin(int target, int start) {
        return copyWithin(target, start, this.size() - 1);
    }

    public Array copyWithin(int target) {
        return copyWithin(target, 0);
    }
    //

    //
    public boolean every(function callback, Object thisArg) {
        callback.thisArg = thisArg;
        for (int i = 0; i < size(); i++) {
            Object element = get(i);
            if (element == null) {
                continue;
            }

            if (!Onekit_JS.is(callback.invoke(element, i, this))) {
                return false;
            }
        }
        return true;
    }

    public boolean every(function callback) {
        return every(callback, null);
    }


    //
    private Array _fill(Object value, int start, int end) {
        start = _index(this, start);
        end = _index(this, end);
        for (int i = start; i >= 0 && i < end && i < size(); i++) {
            set(i, value);
        }
        return this;
    }

    public Array fill(Object value, Object start, Object end) {
        int start_ = Onekit_JS.number(start, 0, 0).intValue();
        int end_ = Onekit_JS.number(end, 0, 0).intValue();
        return _fill(value, start_, end_);
    }

    public Array fill(Object value, Object start) {
        return fill(value, start, this.size());
    }

    public Array fill(Object value) {
        return fill(value, 0);
    }

    //
    public Array filter(function callback, Object thisArg) {
        callback.thisArg = thisArg;
        Array result = new Array();
        for (int i = 0; i < size(); i++) {
            Object element = this.get(i);
            if (element == null) {
                continue;
            }
            if (Onekit_JS.is(callback.invoke(element))) {
                result.add(element);
            }
        }
        return result;
    }

    public Array filter(function callback) {
        return filter(callback, null);
    }

    //
    public Object find(function callback, Object thisArg) {
        callback.thisArg = thisArg;
        for (int i = 0; i < size(); i++) {
            Object element = this.get(i);
            if (Onekit_JS.is(callback.invoke(element, i))) {
                return element;
            }
        }
        return null;
    }

    public Object find(function callback) {
        return find(callback, null);
    }

    //
    public Integer findIndex(function callback, Object thisArg) {
        callback.thisArg = thisArg;
        for (int i = 0; i < size(); i++) {
            Object element = this.get(i);
            if (element == null) {
                continue;
            }
            if (Onekit_JS.is(callback.invoke(element, i))) {
                return i;
            }
        }
        return -1;
    }

    public Integer findIndex(function callback) {
        return findIndex(callback, null);
    }

    //
    private Array _flat(int depth, int current) {
        Array result = new Array();
        for (Object element : this) {
            if (element instanceof Array && current < depth) {
                Array array = (Array) element;
                result = result.concat(array._flat(depth, current + 1));
            } else {
                result.add(element);
            }
        }
        return result;
    }

    public Array flat(int depth) {
        return _flat(depth, 0);
    }

    public Array flat() {
        return flat(1);
    }

    //
    public Array flatMap(function callback, Object thisArg) {
        callback.thisArg = thisArg;
        Array result = new Array();
        for (int i = 0; i < size(); i++) {
            Object element = this.get(i);
            if (element instanceof List) {
                result.addAll((List) callback.invoke(element, i,this));
            } else {
                result.add(callback.invoke(element,i, this));
            }
        }
        return result;
    }

    public Array flatMap(function callback) {
        return flatMap(callback, null);
    }

    //
    public void forEach(function callback, Object thisArg) {
        callback.thisArg = thisArg;
        for (int i = 0; i < size(); i++) {
            Object element = this.get(i);
            if (element == null) {
                continue;
            }
            callback.invoke(element, i, this);
        }

    }

    public void forEach(function callback) {
        forEach(callback, null);
    }

    //
    public Boolean includes(Object valueToFind, Object fromIndex) {
        int index = Onekit_JS.number(fromIndex, 0, 0).intValue();

        for (int i = index; i < size(); i++) {
            if (i < 0) {
                continue;
            }
            Object element = this.get(i);
            if (element.equals(valueToFind)) {
                return new Boolean(true);
            }
        }
        return new Boolean(false);
    }

    //
    public int indexOf(Object searchElement, Object fromIndex) {
        int f = Onekit_JS.number(fromIndex, 0, 0).intValue();
        f = _index(this, f);
        for (int i = f; i < size(); i++) {
            Object temp = get(i);
            if (searchElement.equals(temp)) {
                return i;
            }
        }
        return -1;
    }

    public int indexOf(Object searchElement) {
        return indexOf(searchElement, 0);
    }

    //
    private String _join(String separator) {
        return StringUtils.join(this, separator);
    }

    public String join(Object separator) {
        return new String(_join(separator.toString()));
    }

    public String join() {
        return join(new String(","));
    }

    public int lastIndexOf(Object searchElement, int fromIndex) {
        fromIndex = _index(this, fromIndex);
        for (int i = fromIndex; i >= 0; i--) {
            Object temp = get(i);
            if (searchElement.equals(temp)) {
                return i;
            }
        }
        return -1;
    }

    public int lastIndexOf(Object searchElement) {
        return lastIndexOf(searchElement, size() - 1);
    }

    //
    public Array map(Object callback, Object thisArg) {
        ((function)callback).thisArg = thisArg;
        Array result = new Array();
        for (int i = 0; i < size(); i++) {
            Object element = this.get(i);
            if (element == null) {
                continue;
            }
            result.add(((function)callback).invoke(element));
        }
        return result;
    }

    public Array map(Object callback) {
        return map(callback, null);
    }
    //

    public Object pop() {
        if (this.size() == 0) {
            return null;
        }
        return remove(this.size() - 1);
    }

    public long push(Object... elements) {
        if (elements == null) {
            add(null);
        } else {
            this.addAll(Arrays.asList(elements));
        }
        return this.size();
    }

    public Object reduce(function callback, Object initialValue) {
        boolean flag = (initialValue == null);
        if (flag) {
            initialValue = get(0);
        }
        for (int i = (flag ? 1 : 0); i < size(); i++) {
            Object element = this.get(i);
            if (element == null) {
                continue;
            }
            initialValue = callback.invoke(initialValue, element, i, this);
        }
        return initialValue;
    }

    public Object reduce(function callback) {
        return reduce(callback, null);
    }

    public Object reduceRight(function callback, Object initialValue) {
        boolean flag = initialValue == null;
        if (flag) {
            initialValue = get(size() - 1);
        }
        for (int i = size() - (flag ? 2 : 1); i >= 0; i--) {
            Object element = this.get(i);
            if (element == null) {
                continue;
            }
            initialValue = callback.invoke(initialValue, element, i, this);
        }
        return initialValue;
    }

    public Object reduceRight(function callback) {
        return reduceRight(callback, null);
    }

    public Array reverse() {
        Array temp = new Array();
        for (int i = size() - 1; i >= 0; i--) {
            temp.add(get(i));
        }
        clear();
        addAll(temp);
        return this;
    }

    public Object shift() {
        return this.remove(0);
    }

    private Array _slice(int start, int end) {
        return new Array(this.subList(start, end));
    }

    public Array slice(Object start, Object end) {
        int start_ = Onekit_JS.number(start, 0, 0).intValue();
        int end_ = Onekit_JS.number(end, 0, 0).intValue();
        return _slice(start_, end_);
    }

    public Array slice(Object start) {
        return slice(start, this.size() - 1);
    }

    public Array slice() {
        return slice(0);
    }

    public boolean some(function callback, Object thisArg) {
        for (int i = 0; i < size(); i++) {
            Object element = this.get(i);
            if (element == null) {
                continue;
            }
            if (Onekit_JS.is(callback.invoke(element, i, this))) {
                return true;
            }
        }
        return false;
    }

    public boolean some(function callback) {
        return some(callback, null);
    }

    public Array sort(function comparefunction) {
        Collections.sort(this, (o1, o2) ->  (int)comparefunction.invoke(o1, o2));
        return this;
    }

    public Array sort() {

        Collections.sort(this, (o1, o2) -> {
            String str1 = o1.toString();
            String str2 = o2.toString();
            str1 = str1.length() > 0 ? str1.substring(0, 1) : "";
            str2 = str2.length() > 0 ? str2.substring(0, 1) : "";
            return str1.compareTo(str2);
        });
        return this;
    }

    public Array splice(int start, int deleteCount, Object... items) {
        Array result = new Array(subList(start, start + deleteCount));
        this.removeRange(start, start + deleteCount);
        for (Object element : items) {
            this.add(start++, element);
        }
        return result;
    }

    public Array splice(int start) {
        return splice(start, size() - start);
    }



    //
    public int unshift(Object... elements) {
        int i = 0;
        for (Object element : elements) {
            this.add(i++, element);
        }
        return this.size();
    }

}
package cn.onekit.js;


import cn.onekit.function;

public class Uint8Array extends TypedArray{
    public final static int BYTES_PER_ELEMENT=1;
    public final static String name = "Uint8Array";

    public <TA extends TypedArray> Uint8Array(int length) {
        super(Uint8Array.class, length);
    }

    public <TA extends TypedArray> Uint8Array(TA typedArray) {
        super(typedArray);
    }

    public <TA extends TypedArray> Uint8Array(Object buffer, Object byteOffset, Object length) {
        super(Uint8Array.class, buffer, byteOffset, length);
    }

    public <TA extends TypedArray> Uint8Array(Object buffer, Object byteOffset) {
        super(Uint8Array.class, buffer, byteOffset);
    }

    public <TA extends TypedArray> Uint8Array(Object buffer) {
        super(Uint8Array.class, buffer);
    }

    public <TA extends TypedArray> Uint8Array(Array array) {
        super(Uint8Array.class, array);
    }

    //////////////////////////////////
    public static  Uint8Array from(Set source, function mapFn, Object thisArg) {
        return _from(Uint8Array.class, source, mapFn, thisArg);
    }
    public static Uint8Array of(Object... elements) {
        return _of(Uint8Array.class, elements);
    }
}

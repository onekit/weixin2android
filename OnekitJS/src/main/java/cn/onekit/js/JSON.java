package cn.onekit.js;

import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.onekit.function;

public class JSON {
	private static Gson gson = new Gson();

	public static Object parse(String string) {
		try {
			if (string == null) {
				return null;
			}
			String str = string.trim();
			if (str.equals("")) {
				return null;
			}
			if (str.equals("null")) {
				return null;
			}
			if (str.equalsIgnoreCase("true")) {
				return   true;
			}
			if (str.equalsIgnoreCase("false")) {
				return   false;
			}
			switch (str.substring(0, 1)) {
				case "{":
					return gson.fromJson(str, (Type) HashMap.class);
				case "[":
					return gson.fromJson(str, (Type) List.class);
				default:
					if (string.startsWith("\"") && string.endsWith("\"")) {
						string = string.substring(1, string.length() - 1);
					}
					return  string;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public static String _entry(int depth, String key, Object value, Object replacer, int space){
		return String.format("\"%s\":%s",key,_stringify(depth+1,value,replacer,space));
	}
	public static String _stringify(int depth,Object json, Object replacer, int space) {
		if(space>10){
			space=10;
		}
		if(json==null){
			return null;
		}
		String tab;
		if( depth>0 && space>0){
			String format = "%"+depth*space+"s";
			tab =String.format(format," ");
		}else {
			tab = "";
		}
		if(json instanceof String){
			String string = (String)json;
			return  String.format("\"%s\"",string);
		}
		StringBuilder result = new StringBuilder();
		if(json instanceof List){
			List array = (List) json;
			result.append(tab+"[\r\n");
			int i=0;
			for(Object item: array){
				result.append(_stringify(depth+1,item,replacer,space));
				if(i<array.size()-1) {
					result.append(",");
				}
				result.append("\r\n");
				i++;
			}
			result.append(tab+"]");
		}else if(json instanceof Map){
			Map<String,Object> dict = ( Map<String,Object>) json;
			result.append(tab+"{\r\n");
			int i=0;
			for(String key : dict.keySet()){
				Object value = dict.get(key);
				if(replacer!=null){
					if(replacer instanceof function) {
						result.append(_entry(depth,key,value, replacer,space));
					}else if(replacer instanceof List){
						if(((List) replacer).contains(key)){
							result.append(_entry(depth,key,value,replacer,space));
						}
					}else {
						throw new Error(replacer.toString());
					}
				}else{
					result.append(_entry(depth,key,value,replacer,space));
				}
				if(i<dict.keySet().size()-1) {
					result.append(",");
				}
				result.append("\r\n");
				i++;
			}
			result.append(tab+"}");
		}else{
			String str;
			if(replacer!=null){
				if(replacer instanceof function) {
					str = _stringify( depth+1,((function)replacer).invoke(json),replacer,space);
				}else if(replacer instanceof List){
					str=json.toString();
				}else {
					throw new Error(replacer.toString());
				}
			}else{
				str=json.toString();
			}
			result.append(str);
		}
		return result.toString();
	}
	public static String stringify(Object json, Object replacer,int space) {
		return _stringify( 0,json, replacer,space);
	}
	public static String stringify(Object json, Object replacer,String space) {
		if(space.length()>10){
			space = space.substring(0,10);
		}
		return stringify( json, replacer, space.length());
	}
	public static String stringify(Object json, Object replacer) {
		return stringify(json, replacer, 0);
	}

	public static String stringify(Object json) {
		return stringify(json, null);
	}
}

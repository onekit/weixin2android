package cn.onekit.weixin2android.core.wx.share;

import java.util.Map;
import cn.onekit.function;

import cn.onekit.ACTION1;
import cn.onekit.FUNC2;
import cn.onekit.weixin2android.core.wx.navigate.WxNavigate;

public abstract class WxShare extends WxNavigate {

public Object updateShareMenu(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object showShareMenu(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object showShareImageMenu(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object shareVideoMessage(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object shareFileMessage(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object onCopyUrl(function callback){  return new Object();}
public Object offCopyUrl(function callback){  return new Object();}
public Object hideShareMenu(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object getShareInfo(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object authPrivateMessage(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
}

package cn.onekit.weixin2android.app;

import android.content.Context;
import android.util.AttributeSet;

public class MovableArea extends WeixinElement {
    public MovableArea(Context context) {
        super(context);
    }

    public MovableArea(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    protected void onLayout(boolean b, int i, int i1, int i2, int i3) {

    }
}

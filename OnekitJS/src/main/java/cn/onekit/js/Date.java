package cn.onekit.js;

import android.icu.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;

import java.text.ParsePosition;

import cn.onekit.js.core.Onekit_JS;


public class Date  {

    private java.util.Date THIS;

    public double reduce(Date date) {
        return THIS.getTime() - date.THIS.getTime();
    }

    private Date(java.util.Date THIS) {
        this.THIS = THIS;
    }

    ///////////////////////////////////////////////////////////
    public Date() {
        this(new java.util.Date());
    }
    public Date(Object value) {
        if(value==null){
            THIS = new java.util.Date();
        }
        if(StringUtils.isNumeric(value.toString())) {
            _Date(Long.parseLong(value.toString()));
        }else if(value instanceof String){
            _Date((String)value);
        }
    }
    private void _Date(long value) {
        THIS = new java.util.Date(value);
    }

    private void _Date(String dateString) {
        THIS =new java.util.Date(parse(dateString));
    }

    public Date(Object year, Object month, Object day, Object hours, Object minutes, Object seconds, Object milliseconds) {
        int yearValue = Onekit_JS.number(year,0,0).intValue();
        int monthValue = Onekit_JS.number(month,0,0).intValue();
        int dayValue = Onekit_JS.number(day,0,0).intValue();
        int hoursValue = Onekit_JS.number(hours,0,0).intValue();
        int minutesValue = Onekit_JS.number(minutes,0,0).intValue();
        int secondsValue = Onekit_JS.number(seconds,0,0).intValue();
        int millisecondsValue = Onekit_JS.number(milliseconds,0,0).intValue();
        _Date(new java.util.Date(yearValue >= 100 ? yearValue - 1900 : yearValue, monthValue, dayValue, hoursValue, minutesValue, secondsValue).getTime() + millisecondsValue);
    }

    public Date(Object year, Object monthIndex, Object day, Object hour, Object minutes, Object seconds) {
        this(year, monthIndex, day, hour, minutes, seconds, 0);
    }

    public Date(Object year, Object monthIndex, Object day, Object hour, Object minutes) {
        this(year, monthIndex, day, hour, minutes, 0);
    }

    public Date(Object year, Object monthIndex, Object day, Object hour) {
        this(year, monthIndex, day, hour, 0);
    }

    public Date(Object year, Object monthIndex, Object day) {
        this(year, monthIndex, day, 0);
    }

    public Date(double year, double monthIndex) {
        this(year, monthIndex, 1);
    }

    public Date(double year) {
        this(year, 0);
    }

    public static double now() {

        return new Date().getTime();
    }

    public static long UTC(Object year, Object month, Object date, Object hrs, Object min, Object sec, Object ms) {
        int yearValue = Onekit_JS.number(year,0,0).intValue();
        int monthValue = Onekit_JS.number(month,0,0).intValue();
        int dateValue = Onekit_JS.number(date,0,0).intValue();
        int hrsValue = Onekit_JS.number(hrs,0,0).intValue();
        int minValue = Onekit_JS.number(min,0,0).intValue();
        int secValue = Onekit_JS.number(sec,0,0).intValue();
        int msValue = Onekit_JS.number(ms,0,0).intValue();
        if (yearValue >= 100) {
            yearValue = yearValue-1900;
        }
        java.util.Date localDate = new Date(yearValue, monthValue, dateValue, hrsValue, minValue, secValue, msValue).THIS;
        return _date2utc(localDate);
    }

    private static long _date2utc(java.util.Date date) {
        long localTimeInMillis = date.getTime();
        return localTimeInMillis + date.getTimezoneOffset() * 60 * 1000;
    }

    private static long _utc2date(java.util.Date date) {
        long localTimeInMillis = date.getTime();
        return localTimeInMillis - date.getTimezoneOffset() * 60 * 1000;
    }

    public static long UTC(Object year, Object month, Object date, Object hrs, Object min, Object sec) {
        return UTC(year, month, date, hrs, min, sec, 0);
    }

    public static long UTC(Object year, Object month, Object date, Object hrs, Object min) {
        return UTC(year, month, date, hrs, min, 0);
    }

    public static long UTC(Object year, Object month, Object date, Object hrs) {
        return UTC(year, month, date, hrs, 0);
    }

    public static long UTC(Object year, Object month, Object date) {
        return UTC(year, month, date, 0);
    }

    public static long UTC(Object year, Object month) {
        return UTC(year, month, 1);
    }

    public static long UTC(Object year) {
        return UTC(year, 0);
    }

    public static long parse(String dateString) {
        SimpleDateFormat sdf = new SimpleDateFormat();

        String[] patterns = new String[]{
                "EEE, d MMM yyyy HH:mm:ss 'Z'",
                "yyyy-MM-dd'Object'HH:mm:ss.SSS'Z'",
                "yyyy-MM-dd'Object'HH:mm:ss",
                "yyyy-MM-dd'Object'HH:mm:ss.SSSXXX",
                "MMM dd, yy hh:mm:ss"};
        for (String pattern : patterns) {
            sdf.applyPattern(pattern);
            sdf.setLenient(false);//设置解析日期格式是否严格解析日期
            ParsePosition pos = new ParsePosition(0);
            java.util.Date date = sdf.parse(dateString, pos);
            if (date != null) {
                return date.getTime();
            }
        }
        long tick = java.util.Date.parse(dateString);
        if (tick != 0) {
            return tick;
        }
        throw new java.lang.Error(dateString);
    }

    public double getDate() {
        return THIS.getDate();
    }

    public double getDay() {
        return THIS.getDay();
    }

    public double getFullYear() {
        int year = THIS.getYear();
        if (year < 1900) {
            year += 1900;
        }
        return year;
    }

    public double getHours() {
        return THIS.getHours();
    }

    public double getMilliseconds() {
        int result = (int) (THIS.getTime() % 1000);
        if (result < 0) {
            result += 1000;
        }
        return result;
    }

    public double getMinutes() {
        return THIS.getMinutes();
    }

    public double getMonth() {
        return THIS.getMonth();
    }

    public double getSeconds() {
        return THIS.getSeconds();
    }

    public double getTime() {
        return THIS.getTime();
    }

    public double getTimezoneOffset() {
        return THIS.getTimezoneOffset();
    }

    public double getUTCDate() {
        return new Date(_date2utc(THIS)).getDate();
    }

    public double getUTCDay() {
        return new Date(_date2utc(THIS)).getDay();
    }

    public double getUTCFullYear() {
        return new Date(_date2utc(THIS)).getFullYear();
    }

    public double getUTCHours() {
        return new Date(_date2utc(THIS)).getHours();
    }

    public double getUTCMilliseconds() {
        return new Date(_date2utc(THIS)).getMilliseconds();
    }

    public double getUTCMinutes() {
        return new Date(_date2utc(THIS)).getMinutes();
    }

    public double getUTCMonth() {
        return new Date(_date2utc(THIS)).getMonth();
    }

    public double getUTCSeconds() {
        return new Date(_date2utc(THIS)).getSeconds();
    }

    //被重写为void
    public double setDate(Object day) {
        int dayValue = Onekit_JS.number(day,0,0).intValue();
        THIS.setDate(dayValue);
        return this.getTime();
    }

    public double setFullYear(Object yearValue, Object monthValue, Object dayValue) {
        setYear(yearValue);
        setMonth(monthValue);
        setDate(dayValue);
        return this.getTime();
    }

    public double setFullYear(Object yearValue, Object monthValue) {
        return setFullYear(yearValue, monthValue, 1);
    }

    public double setFullYear(Object yearValue) {
        return setFullYear(yearValue, 0);
    }

    public double setHours(Object hours, Object minutes, Object seconds, Object ms) {
        int hoursValue = Onekit_JS.number(hours,0,0).intValue();
        int minutesValue = Onekit_JS.number(minutes,0,0).intValue();
        int secondsValue = Onekit_JS.number(seconds,0,0).intValue();
        int msValue = Onekit_JS.number(ms,0,0).intValue();
        THIS.setHours(hoursValue);
        THIS.setMinutes(minutesValue);
        THIS.setSeconds(secondsValue);
        return setMilliseconds(msValue);
    }

    public double setHours(double hoursValue, double minutesValue, double secondsValue) {
        return setHours(hoursValue, minutesValue, secondsValue, getMilliseconds());
    }

    public double setHours(double hoursValue, double minutesValue) {
        return setHours(hoursValue, minutesValue, getSeconds());
    }

    //被重写为void
    public double setHours(double hoursValue) {
        return setHours(hoursValue, getMinutes());
    }

    private static java.util.Date _setMilliseconds(java.util.Date date, Object milliseconds) {
        int millisecondsValue = Onekit_JS.number(milliseconds,0,0).intValue();
        long ms = date.getTime();
        ms = ms - ms % 1000;
        ms += millisecondsValue;
        date.setTime(ms);
        return date;
    }

    public double setMilliseconds(Object millisecondsValue) {
        THIS = _setMilliseconds(THIS, millisecondsValue);
        return this.getTime();
    }

    public double setMinutes(Object minutes, Object seconds, Object ms) {
        int minutesValue = Onekit_JS.number(minutes,0,0).intValue();
        int secondsValue = Onekit_JS.number(seconds,0,0).intValue();
        int msValue = Onekit_JS.number(ms,0,0).intValue();
        THIS.setMinutes(minutesValue);
        THIS.setSeconds(secondsValue);
        return setMilliseconds(msValue);
    }

    public double setMinutes(Object minutesValue, Object secondsValue) {
        return setMinutes(minutesValue, secondsValue, getMilliseconds());
    }

    //被重写方法为void
    public double setMinutes(Object minutesValue) {
        return setMinutes(minutesValue, getSeconds());
    }

    public double setMonth(Object month, Object day) {
        int monthValue = Onekit_JS.number(month,0,0).intValue();
        int dayValue = Onekit_JS.number(day,0,0).intValue();
        THIS.setMonth(monthValue);
        THIS.setDate(dayValue);
        return this.getTime();
    }

    public double setMonth(Object monthValue) {
        return setMonth(monthValue, getDate());
    }


    public double setSeconds(Object seconds, Object msValue) {
        int secondsValue = Onekit_JS.number(seconds,0,0).intValue();
        THIS.setSeconds(secondsValue);
        return setMilliseconds(msValue);
    }

    public double setSeconds(Object secondsValue) {
        return setSeconds(secondsValue, getMilliseconds());
    }

    public double setTime(long timeValue) {
        THIS.setTime(timeValue);
        return this.getTime();
    }

    public double setUTCDate(double day) {
        int dayValue = Onekit_JS.number(day,0,0).intValue();
        java.util.Date utc = new java.util.Date(_date2utc(THIS));
        utc.setDate(dayValue);
        THIS = new java.util.Date(_utc2date(utc));
        return this.getTime();
    }

    public double setUTCFullYear(Object year, Object month, Object day) {
        int yearValue = Onekit_JS.number(year,0,0).intValue();
        int monthValue = Onekit_JS.number(month,0,0).intValue();
        int dayValue = Onekit_JS.number(day,0,0).intValue();
        if (yearValue >= 99) {
            yearValue = yearValue-1900;
        }
        java.util.Date utc = new java.util.Date(_date2utc(THIS));
        utc.setYear(yearValue);
        utc.setMonth(monthValue);
        utc.setDate(dayValue);
        THIS = new java.util.Date(_utc2date(utc));
        return this.getTime();
    }

    public double setUTCFullYear(Object yearValue, Object monthValue) {
        return setUTCFullYear(yearValue, monthValue, getUTCDay());
    }

    public double setUTCFullYear(Object yearValue) {
        return setUTCFullYear(yearValue, getUTCMonth());
    }

    public double setUTCHours(Object hours, Object minutes, Object seconds, Object ms) {
        int hoursValue = Onekit_JS.number(hours,0,0).intValue();
        int minutesValue = Onekit_JS.number(minutes,0,0).intValue();
        int secondsValue = Onekit_JS.number(seconds,0,0).intValue();
        int msValue = Onekit_JS.number(ms,0,0).intValue();
        java.util.Date utc = new java.util.Date(_date2utc(THIS));
        utc.setHours(hoursValue);
        utc.setMinutes(minutesValue);
        utc.setSeconds(secondsValue);
        utc = _setMilliseconds(utc, msValue);
        THIS = new java.util.Date(_utc2date(utc));
        return THIS.getTime();
    }

    public double setUTCHours(Object hoursValue, Object minutesValue, Object secondsValue) {
        return setUTCHours(hoursValue, minutesValue, secondsValue, getUTCMilliseconds());
    }

    public double setUTCHours(Object hoursValue, Object minutesValue) {
        return setUTCHours(hoursValue, minutesValue, getUTCSeconds());
    }

    public double setUTCHours(Object hoursValue) {
        return setUTCHours(hoursValue, getUTCMinutes());
    }

    public double setUTCMilliseconds(Object milliseconds) {
        int millisecondsValue = Onekit_JS.number(milliseconds,0,0).intValue();
        java.util.Date utc = new java.util.Date(_date2utc(THIS));
        utc = _setMilliseconds(utc, millisecondsValue);
        THIS = new java.util.Date(_utc2date(utc));
        return THIS.getTime();
    }

    public double setUTCMinutes(Object minutes, Object seconds, Object ms) {
        int minutesValue = Onekit_JS.number(seconds,0,0).intValue();
        int secondsValue = Onekit_JS.number(seconds,0,0).intValue();
        int msValue = Onekit_JS.number(ms,0,0).intValue();
        java.util.Date utc = new java.util.Date(_date2utc(THIS));
        utc.setMinutes(minutesValue) ;
        utc.setSeconds(secondsValue);
        utc = _setMilliseconds(utc, msValue);
        THIS = new java.util.Date(_utc2date(utc));
        return THIS.getTime();
    }

    public double setUTCMinutes(Object minutesValue, Object secondsValue) {
        return setUTCMinutes(minutesValue, secondsValue, getUTCMilliseconds());
    }

    public double setUTCMinutes(Object minutesValue) {
        return setUTCMinutes(minutesValue, getUTCSeconds());
    }

    public double setUTCMonth(Object month, Object day) {
        int monthValue = Onekit_JS.number(month,0,0).intValue();
        int dayValue = Onekit_JS.number(day,0,0).intValue();
        java.util.Date utc = new java.util.Date(_date2utc(THIS));
        utc.setMonth(monthValue);
        utc.setDate(dayValue);
        THIS = new java.util.Date(_utc2date(utc));
        return THIS.getTime();
    }

    public double setUTCMonth(double monthValue) {
        return setUTCMonth(monthValue, getUTCDay());
    }

    public double setUTCSeconds(double seconds, double ms) {
        int secondsValue = Onekit_JS.number(seconds,0,0).intValue();
        int msValue = Onekit_JS.number(ms,0,0).intValue();
        java.util.Date utc = new java.util.Date(_date2utc(THIS));
        utc.setSeconds(secondsValue);
        utc = _setMilliseconds(utc, msValue);
        THIS = new java.util.Date(_utc2date(utc));
        return THIS.getTime();
    }

    public double setUTCSeconds(double secondsValue) {
        return setUTCSeconds(secondsValue, getUTCMilliseconds());
    }

    public double setYear(Object year) {
        int yearValue = Onekit_JS.number(year,0,0).intValue();
        if (yearValue >= 99) {
            yearValue =   yearValue -1900;
        }
        THIS.setYear(yearValue);
        return this.getTime();
    }

    public String toDateString() {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM d yyyy");
        return new String(sdf.format(THIS));

    }

    //重写方法
    public String toGMTString() {
        return new String(THIS.toGMTString());
    }

    public String toISOString() {
        Date utc = new Date(_date2utc(THIS));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'Object'HH:mm:ss.SSSZ");
        return new String(sdf.format(utc.THIS));
    }


    public String toJSON() {
        return toISOString();
    }

    public String toLocaleDateString(String locales, java.util.Map options) {
        return new String("");
    }

    public String toLocaleDateString(String locales) {
        return toLocaleDateString(locales, new java.util.HashMap<>());
    }

    public String toLocaleDateString() {
        return toLocaleDateString("");
    }

    public String toLocaleFormat(Object formatString) {
        return new String("");
    }

    public String toLocaleString(Object locales, Object options) {
        return new String("");
    }

    public String toLocaleTimeString(Object locales, Object options) {
        return new String("");
    }

    public String toSource() {
        return null;
    }

    public String ToString() {

        return new String(new SimpleDateFormat("EEE MMM dd yyyy hh:mm:ss 'GMT'Z").format(THIS));
    }



    public String toTimeString() {
        return null;
    }

    public String toUTCString() {
        return new String(new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss 'GMT'").format(THIS));
    }

    public double getYear() {
        return THIS.getYear();
    }

}
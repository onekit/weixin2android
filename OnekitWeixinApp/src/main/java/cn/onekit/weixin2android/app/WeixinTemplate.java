package cn.onekit.weixin2android.app;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;

import cn.onekit.vue.Vue;
import cn.onekit.w3c.Template;

public abstract class WeixinTemplate extends Template {
    public WeixinTemplate(Context context) {
        super(context);
    }

    public WeixinTemplate(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public abstract void ui(final ViewGroup ui, final Object data, final Vue vue);
}

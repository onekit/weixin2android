package cn.onekit.w3c;

import java.util.Map;


public class Event  {
    private Element currentTarget;
    private Map<String,Object> detail;
    //private Map<String,Object>  mark;
  //  private boolean mut;
    private Element target;
    private int timeStamp;
    private String type;
    //
    public Event(
            String type,
            Map<String,Object> detail,
     //       Map<String,Object>  mark,
      //   boolean mut,
            Element currentTarget,
            Element target,
         int timeStamp)
    {
        this.currentTarget=currentTarget;
        this.detail=detail;
       // this.mark=mark;
     //   this.mut=mut;
        this.target=target;
        this.timeStamp=timeStamp;
        this.type=type;
    }
    //
    public Element getCurrentTarget(){
        return currentTarget;
    }
    public Map<String,Object> getDetail(){
        return detail;
    }
   /* public Map<String,Object> getMark(){
        return mark;
    }*/
   /* public boolean getMut(){
        return mut;
    }*/
    public Element getTarget(){
        return target;
    }
    public int getTimeStamp(){
        return timeStamp;
    }
    public String getType(){
        return type;
    }
   /* public Event(String type,Map<String,Object> detail){
        this.type=type;
        this.detail = detail;
    }*/

    @Override
    public String toString() {
        return String.format("\"currentTarget\":%s,\"detail\":%s,\"mark\":%s,\"mut\":%s,\"target\":%s,\"timeStamp\":%s,\"type\":\"%s\"}",
                currentTarget,
                detail,
                "mark",
                "mut",
                target,
                timeStamp,
                type);
    }


}

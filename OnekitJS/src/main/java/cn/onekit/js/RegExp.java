package cn.onekit.js;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.onekit.js.core.Onekit_JS;


public class RegExp  {

    public String getPattern() {
        return THIS.pattern();
    }


    

    public static class Match  {
        Array _finds;


        public Object get(Object index) {
            return _finds.get(Onekit_JS.number(index,0,0).intValue());
        }



        

        Array _groups;

        public Array getGroups() {
            return _groups;
        }

        int _index;

        public int getIndex() {
            return _index;
        }

        String _input;

        public String getInput() {
            return _input;
        }

        int _length;

        public int getLength() {
            return _length;
        }

        public Match(Array finds, Array groups, int index, String input, int length) {
            _finds = finds;
            _groups = groups;
            _index = index;
            _input = input;
            _length = length;
        }

        @Override
        public String toString() {
            StringBuilder result = new StringBuilder();
            result.append("[");
            for(int i=0;i<getLength();i++) {
                if(i>0){
                    result.append(",");

                }
                result.append(String.format("%d:\"%s\"", i,get(i)));
            }
            result.append("]");
            result.append(String.format("groups:%s,", Onekit_JS.toString(getGroups())));
            result.append(String.format("index:%d,", getIndex()));
            result.append(String.format("input:\"%s\",", getInput()));
            result.append(String.format("length:%d", getLength()));
            return result.toString();
        }
    }
    ////////////////////////////////////////
    public static Array $s=new Array();
    static java.util.Map $=new java.util.HashMap<>();
    public Pattern THIS;
    String _flags;
    int _init(String flags){
        int flag = 0;
        if (flags == null) {
            flags = "";
        }
        for (char f : flags.toCharArray()) {
            switch (f) {
                case 'i':
                    flag += Pattern.CASE_INSENSITIVE;
                    break;
                case 'm':
                    flag += Pattern.MULTILINE;
                    break;
                case 'u':
                    flag += Pattern.UNICODE_CASE;
                    break;
                case 's':
                    flag += Pattern.DOTALL;
                    break;
                default:
                    break;
            }
        }
        char[] array = flags.toCharArray();
        Arrays.sort(array);
        _flags = String.copyValueOf(array);
        return flag;
    }
    public RegExp(String pattern,String flags) {
        int flag = _init(flags);
        THIS = Pattern.compile(pattern, flag);
    }
    public RegExp(String pattern){
        this(pattern,null);
    }
    public static Object _get(String key) {
        return $.get(key);
    }
    /////////////////
    public static Object get$1() {
        return $s.get(0);
    }
    public static Object get$2() {
        return $s.get(1);
    }
    public static Object get$3() {
        return $s.get(2);
    }
    public static Object get$4() {
        return $s.get(3);
    }
    public static Object get$5() {
        return $s.get(4);
    }
    public static Object get$6() {
        return $s.get(5);
    }
    public static Object get$7() {
        return $s.get(6);
    }
    public static Object get$8() {
        return $s.get(7);
    }
    public static Object get$9() {
        return $s.get(8);
    }
    public static Object getInput() {
        return _get("$_");
    }
    public static Object getLastMatch() {
        return $.get("$&");
    }
    public static Object getLastParen() {
        return $.get("$+");
    }
    public static Object getLeftContext() {
        return $.get("$`");
    }
    public  String getFlags() {
        return _flags;
    }
    public  final boolean getGlobal(){
        return getFlags().contains("g");
    }
    public  final boolean getIgnoreCase(){
     return getFlags().contains("i");
    }
    public  final boolean getMultiline(){
     return getFlags().contains("m");
    }
   /* public  String getSource(){
        return THIS.pattern();
    }*/
    public  final boolean getSticky(){
        return getFlags().contains("y");
    }
    public  final boolean getUnicode(){
        return getFlags().contains("u");
    }
    public static final Object getRightContext(){
        return $.get("$'");
    }
    int _lastIndex=0;
    public int getLastIndex(){
        return _lastIndex;
    }
    //////////////////////////////////////////
    public void compile(String pattern,String flags){
        int flag = _init(flags);
        THIS = Pattern.compile(pattern, flag);
    }
    public Array exec(String str){
        Array result = null;
        Matcher matcher = THIS.matcher(str);
        if (matcher.find(_lastIndex)) {
            if(result==null){
                result=new Array();
            }
            _lastIndex = matcher.end();
            for (int i = 0; i <= matcher.groupCount(); i++) {
                String group = matcher.group(i);
                result.add(group);
            }
        }
        return result;
    }
    public  boolean test(String str) {

        Matcher matcher = THIS.matcher(str);
        boolean result = false;
        while (matcher.find()) {
            result = true;
            $.put("$_", str);
            $.put("$`", str.substring(0, matcher.start()));
            $.put("$'", str.substring(matcher.end()));
            for (int i = 0; i <= matcher.groupCount(); i++) {
                String group = matcher.group(i);
                $.put("$&",  group);
                $.put("$+",  group);
            }
        }
        return result;
    }

    @Override
    public String toString() {
        return String.format("/%s/%s",THIS,getFlags());
    }

}

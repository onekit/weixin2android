package cn.onekit.weixin2android.core.wx.analysis;

import java.util.List;

import cn.onekit.weixin2android.CacheManager;
import cn.onekit.weixin2android.core.wx.storage.WxStorage;

public abstract class WxAnalysis extends WxStorage {
    public Object reportMonitor(String name, double value) {
            return new Object();
    }

    public Object reportEvent(String eventId, Object data) {
           return new Object();
    }

    public Object reportAnalytics(String eventName, Object value) {
           return new Object();
    }

    public CacheManager getExptInfoSync(List<String> keys) {
           return new CacheManager();
    }
}

package cn.onekit.weixin2android.core.wx.basic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.onekit.ACTION1;
import cn.onekit.FUNC2;
import cn.onekit.function;
import cn.onekit.js.ArrayBuffer;
import cn.onekit.weixin2android.RealtimeLogManager;
import cn.onekit.weixin2android.UserCryptoManager;
import cn.onekit.weixin2android.core.wx.Wx;
import cn.onekit.weixin2android.core.wx.device.WxDevice;

public abstract class WxMiniprogram extends WxDebug {

    public Object getLaunchOptionsSync() {
        return new Object();
    }

    public Object getEnterOptionsSync() {
        return new Object();
    }

    public Object onUnhandledRejection(function callback) {
        return new Object();
    }

    public Object onThemeChange(Object object) {
        return new Object();
    }

    public Object onPageNotFound(function callback) {
        return new Object();
    }

    public Object onLazyLoadError(function callback) {
        return new Object();
    }

    public Object onError(function callback) {
        return new Object();
    }

    public Object onAudioInterruptionEnd(function callback) {
        return new Object();
    }

    public Object onAudioInterruptionBegin(function callback) {
        return new Object();
    }

    public Object onAppShow(function callback) {
        return new Object();
    }

    public Object onAppHide(function callback) {
        return new Object();
    }

    public Object offUnhandledRejection(function callback) {
        return new Object();
    }

    public Object offThemeChange(function callback) {
        return new Object();
    }

    public Object offPageNotFound(function callback) {
        return new Object();
    }

    public Object offError(function callback) {
        return new Object();
    }

    public Object offAudioInterruptionEnd(function callback) {
        return new Object();
    }

    public Object offAudioInterruptionBegin(function callback) {
        return new Object();
    }

    public Object offAppShow(function callback) {
        return new Object();
    }

    public Object offAppHide(function callback) {
        return new Object();
    }

}

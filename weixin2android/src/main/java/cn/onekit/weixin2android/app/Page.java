package cn.onekit.weixin2android.app;

import android.content.Context;
import android.util.AttributeSet;

public class Page extends WeixinElement {
    public Page(Context context) {
        super(context);
    }

    public Page(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

}

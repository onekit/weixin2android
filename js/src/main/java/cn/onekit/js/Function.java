package cn.onekit.js;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;

import cn.onekit.js.function;

public  class function implements function {
    Object object;
    Method method;
    public Object thisArg;

    public function() {

    }

    public function(Class clazz, String methodName, Class<Object>... types) {
        try {
            method = clazz.getMethod(methodName, types);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public function(Object object, String methodName, Class<Object>... types) {
        this(object.getClass(), methodName, types);
        this.object = object;
    }

    public Optional<Object> invoke(Object... arguments) {
        try {
            return null;//method.invoke(object, Arrays.stream(arguments).toArray());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}


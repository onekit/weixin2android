package cn.onekit.weixin2android;

public class WifiInfo {
    public String SSID;
    public String BSSID;
    public boolean secure;
    public int signalStrength;
}

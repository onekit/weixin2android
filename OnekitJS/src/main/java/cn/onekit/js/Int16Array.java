package cn.onekit.js;


import cn.onekit.function;

public class Int16Array extends TypedArray{
    public final static int BYTES_PER_ELEMENT=2;
    public final static String name="Int16Array";
    public Int16Array(Object length) {
        super(Int16Array.class,length);
    }

    public Int16Array(TypedArray typedArray) {
        super(typedArray);
    }

    public Int16Array(Object buffer, Object byteOffset, Object length) {
        super(Int16Array.class,buffer, byteOffset, length);
    }

    public Int16Array(Object buffer, Object byteOffset) {
        super(Int16Array.class,buffer, byteOffset);
    }

    public Int16Array(Array array) {
        super(Int16Array.class,array);
    }
    //////////////////////////////////
    public static  Int16Array from(Set source, function mapFn, Object thisArg) {
        return _from(Int16Array.class, source, mapFn, thisArg);
    }
    public static Int16Array of(Object... elements) {
        return _of(Int16Array.class, elements);
    }
}

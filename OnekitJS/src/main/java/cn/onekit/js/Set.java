package cn.onekit.js;

import java.util.HashSet;
import java.util.Random;

import cn.onekit.js.core.Onekit_JS;
import cn.onekit.function;

public class Set implements Iterable<Object> {

    int _hashCode =  new Random().nextInt();
    @Override
    public int hashCode() {
        return _hashCode;
    }
    java.util.Set<Object> _THIS;

    public Set(Array array) {
        _THIS = new HashSet(array);
    }

    public Set(String array) {
        this(Onekit_JS.string2Array(array));
    }

    public Set() {
        _THIS = new HashSet();
    }

    /////////////////////////////////
    public int getSize() {
        return _THIS.size();
    }

    public Set add(Object value) {
        _THIS.add(value);
        return this;
    }

    public void clear() {
        _THIS.clear();
    }

    public boolean delete(Object value) {
        return _THIS.remove(value);
    }
    public  void forEach(function callback, Object THIS) {
        callback.thisArg = THIS;
        for(Object item : _THIS){
            callback.invoke(item,item,this);
        }
    }
    public void forEach(function callback) {
        forEach(callback,null);
    }
    public boolean has(Object value) {
        return _THIS.contains(value);
    }


    @Override
    public String toString() {
        StringBuilder result=new StringBuilder();
        result.append("Set [");
        int i=0;
        for (Object item : _THIS) {
            if (i++ > 0) {
                result.append(",");
            }
            result.append(item);
        }
        result.append("]");
        return result.toString();
    }



    @Override
    public java.util.Iterator iterator() {
        return _THIS.iterator();
    }

    
}

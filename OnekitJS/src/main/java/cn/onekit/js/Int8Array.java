package cn.onekit.js;




public class Int8Array extends TypedArray{
    public final static int BYTES_PER_ELEMENT=1;
    public final static String name="Int8Array";
    public Int8Array(Object length) {
        super(Int8Array.class,length);
    }


    public Int8Array(Object buffer, Object byteOffset, Object length) {
        super(Int8Array.class,buffer, byteOffset, length);
    }

    public Int8Array(Object buffer, Object byteOffset) {
        super(Int8Array.class,buffer, byteOffset);
    }


    public Int8Array(Array array) {
        super(Int8Array.class,array);
    }
    //////////////////////////////////
    public static  Int8Array from(Object source, Object mapFn, Object thisArg) {
        return _from(Int8Array.class, source, mapFn, thisArg);
    }


    public static Int8Array of(Object... elements) {
        return _of(Int8Array.class, elements);
    }

}

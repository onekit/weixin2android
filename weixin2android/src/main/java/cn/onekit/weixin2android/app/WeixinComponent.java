package cn.onekit.weixin2android.app;

import android.content.Context;
import android.util.AttributeSet;

import java.util.HashMap;
import java.util.Map;

import cn.onekit.weixin2android.app.core.WeixinFile;

public  abstract class WeixinComponent extends WeixinElement implements WeixinFile {
    private Map<String, Object> object=new HashMap<>();

    public WeixinComponent(Context context) {
        super(context);
    }

    public WeixinComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    // Map<Integer, java.util.Map<String, Object>> all_object = new HashMap<>();

    protected void Component(Map<String, Object> object) {
        this.object=object;
        //all_object.put(this.hashCode(), object);
    }

    protected void setData(Map<String, Object> data) {

    }

     Object get(String key) {
         return object.get(key);
    //    return all_object.get(this.hashCode()).get(key);
    }
}

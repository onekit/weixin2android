package cn.onekit;

public  interface FUNC2<T1,T2,TResult> {

    TResult invoke(T1 arg1, T2 arg2);
}

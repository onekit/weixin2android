package cn.onekit.weixin2android.core.wx.ui;

import java.util.Map;

import cn.onekit.ACTION1;
import cn.onekit.FUNC2;
import cn.onekit.function;
import cn.onekit.weixin2android.core.wx.share.WxShare;

public abstract class WxInteraction extends WxShare {

public Object showToast(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object showModal(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object showLoading(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object showActionSheet(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object hideToast(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object hideLoading(function callback){  return new Object();}
public Object enableAlertBeforeUnload(function callback){  return new Object();}
public Object disableAlertBeforeUnload(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}

public Object showNavigationBarLoading(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object setNavigationBarTitle(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object setNavigationBarColor(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object hideNavigationBarLoading(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object hideHomeButton(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object setBackgroundTextStyle(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object setBackgroundColor(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object showTabBarRedDot(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object showTabBar(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object setTabBarStyle(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object setTabBarItem(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object setTabBarBadge(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object removeTabBarBadge(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object hideTabBarRedDot(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object hideTabBar(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object loadFontFace(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object stopPullDownRefresh(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object startPullDownRefresh(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object pageScrollTo(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object createAnimation(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object setTopBarText(Map<String,Object> object){     ACTION1<Map<String,Object>> success = (object!=null && object.containsKey("success"))?(ACTION1<Map<String,Object>>)object.get("success"):null;
        ACTION1<Map<String,Object>> fail = (object!=null && object.containsKey("fail"))?(ACTION1<Map<String,Object>>)object.get("fail"):null;
        ACTION1<Map<String,Object>> complete = (object!=null && object.containsKey("complete"))?(ACTION1<Map<String,Object>>)object.get("complete"):null;
        PROMISE(new FUNC2<ACTION1<Map<String, Object>>, ACTION1<Map<String, Object>>, Object>() {
            @Override
            public Object invoke(ACTION1<Map<String, Object>> arg1, ACTION1<Map<String, Object>> arg2) {
                return null;
            }
        },success,fail,complete);return new Object();}
public Object nextTick(function callback){  return new Object();}
public Object getMenuButtonBoundingClientRect(){ return new Object();}
public Object setWindowSize(function callback){  return new Object();}
public Object onWindowResize(function callback){  return new Object();}
public Object offWindowResize(function callback){  return new Object();}
}

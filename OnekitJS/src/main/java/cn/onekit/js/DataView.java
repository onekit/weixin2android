package cn.onekit.js;


import cn.onekit.js.core.Onekit_JS;

/**
 * Created by zhangjin on 2017/10/30.
 */

public class DataView  {
    private final int _byteLength;
    private ArrayBuffer _buffer;
    private int _byteOffset;

    ////////////////////////////////////////////////////
    public DataView(Object buffer, Object byteOffset, Object byteLength) {
        _byteOffset = Onekit_JS.number(byteOffset,0,0).intValue();
        _byteLength = Onekit_JS.number(byteLength,0,0).intValue();
        _buffer = (ArrayBuffer)buffer;
    }

    public DataView(Object buffer, Object byteOffset) {
        this(buffer, byteOffset, ((ArrayBuffer)buffer).getByteLength()- (Onekit_JS.number(byteOffset,0,0).intValue()));
    }

    public DataView(Object buffer) {
        this(buffer, 0);
    }

    //////////////////////////////////////////////

    public ArrayBuffer getBuffer() {
        return _buffer;
    }

    public int getByteLength() {
        return _byteLength;
    }

    public int getByteOffset() {
        return _byteOffset;
    }
    ////////////////////////////////////////////////


    public float getFloat32(Object byteOffset, Object littleEndian) {
        if(littleEndian==null){
            littleEndian = new Boolean(false) ;
        }
        return  _get(byteOffset, "Float32", 4, (Boolean)littleEndian).floatValue();
    }
 

    public Double getFloat64(Object byteOffset, Object littleEndian) {
        if(littleEndian==null){
            littleEndian = new Boolean(false) ;
        }
        return _get(byteOffset, "Float64", 8, (Boolean)littleEndian).doubleValue();
    }

    public Short getInt16(Object byteOffset, Object littleEndian) {
        if(littleEndian==null){
            littleEndian = new Boolean(false) ;
        }
        return  _get(byteOffset, "Int16", 2, (Boolean)littleEndian).shortValue();
    }
    

    public Integer getInt32(Object byteOffset, Object littleEndian) {
        if(littleEndian==null){
            littleEndian = new Boolean(false) ;
        }
        return  _get(byteOffset, "Int32", 4, (Boolean)littleEndian).intValue();
    }

    public Byte getInt8(Object byteOffset) {
        return  _get(byteOffset, "Int8", 1, false).byteValue();
    }

    public Integer getUint16(Object byteOffset, Object littleEndian) {
        if(littleEndian==null){
            littleEndian = new Boolean(false) ;
        }
        return  _get(byteOffset, "Uint16", 2, (Boolean)littleEndian).intValue();
    }
    public Long getUint32(Object byteOffset, Object littleEndian) {
        if(littleEndian==null){
            littleEndian = new Boolean(false) ;
        }
        return  _get(byteOffset, "Uint32", 4, (Boolean)littleEndian).longValue();
    }

    public Short getUint8(Object byteOffset) {
        return  _get(byteOffset, "Uint8", 1, false).shortValue();
    }

    public void setFloat32(Object byteOffset, Object value, Object littleEndian) {
        if(littleEndian==null){
            littleEndian = new Boolean(false) ;
        }
        _set(byteOffset, value, "Float32", 4, (Boolean)littleEndian);
    }

    public void setFloat64(Object byteOffset, Object value, Object littleEndian) {
        if(littleEndian==null){
            littleEndian = new Boolean(false) ;
        }
        _set(byteOffset, value, "Float64", 8, (Boolean)littleEndian);
    }


    public void setInt16(Object byteOffset, Object value, Object littleEndian) {
        if(littleEndian==null){
            littleEndian = new Boolean(false) ;
        }
        _set(byteOffset, value, "Int16", 2, (Boolean)littleEndian);
    }

    public void setInt32(Object byteOffset, Object value, Object littleEndian) {
        if(littleEndian==null){
            littleEndian = new Boolean(false) ;
        }
        _set(byteOffset, value, "Int32", 4, (Boolean)littleEndian);
    }

    public void setInt8(Object byteOffset, Object value) {
        
        _set(byteOffset, value, "Int8", 1, false);
    }

    

    public void setUint16(Object byteOffset, Object value, Object littleEndian) {
        if(littleEndian==null){
            littleEndian = new Boolean(false) ;
        }
        _set(byteOffset, value, "Uint16", 2, (Boolean)littleEndian);
    }
    public void setUint32(Object byteOffset, Object value, Object littleEndian) {
        if(littleEndian==null){
            littleEndian = new Boolean(false) ;
        }
        _set(byteOffset, value, "Uint32", 4, (Boolean)littleEndian);
    }

    public void setUint8(Object byteOffset, Object value, Object littleEndian) {
        if(littleEndian==null){
            littleEndian = new Boolean(false) ;
        }
        _set(byteOffset, value, "Uint8", 1, (Boolean)littleEndian);
    }

    private java.lang.Number _get(Object byteOffset, String type, int BYTES_PER_ELEMENT, boolean littleEndian) {
        
        return Onekit_JS.bytes2number(_buffer._data, type, BYTES_PER_ELEMENT,getByteOffset()+ Onekit_JS.number(byteOffset,0,0).intValue());
    }

    private <T extends Number>  void _set(Object byteOffset,Object value, String type, int BYTES_PER_ELEMENT, boolean littleEndian) {
        Onekit_JS.number2bytes(_buffer._data, type, BYTES_PER_ELEMENT,_byteOffset+ Onekit_JS.number(byteOffset,0,0).intValue(), value);
    }


    
}


















































